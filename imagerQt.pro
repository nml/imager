#-------------------------------------------------
#
# Project created by QtCreator 2019-03-20T11:30:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = ImagerQt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# No debug output
CONFIG(release, debug|release): DEFINES += QT_NO_DEBUG_OUTPUT

CONFIG += c++11

SOURCES += \
        $$PWD/Sources/main.cpp \
    $$PWD/Sources/MainWindow.cpp \
    $$PWD/Sources/setup.cpp \
    $$PWD/Sources/worker.cpp \
    $$PWD/Sources/plotdata.cpp \
    $$PWD/Sources/plotwindow.cpp \
    $$PWD/Sources/qcustomcanvas.cpp \
    $$PWD/Sources/formentry.cpp \
    $$PWD/Sources/qcustomplot.cpp \
    $$PWD/Sources/customplotzoom.cpp

HEADERS += \
    $$PWD/Headers/MainWindow.h \
    $$PWD/Headers/setup.h \
    $$PWD/Headers/worker.h \
    $$PWD/Headers/plotdata.h \
    $$PWD/Headers/plotwindow.h \
    $$PWD/Headers/qcustomcanvas.h \
    $$PWD/Headers/formentry.h \
    $$PWD/Headers/qcustomplot.h \
    $$PWD/Headers/customplotzoom.h

FORMS += \
    MainWindow.ui \
    formEntry.ui \
    plotcanvas.ui \
    plotwindow.ui

gcc {
  QMAKE_LFLAGS += -fopenmp
  QMAKE_CXXFLAGS += -fopenmp
  #QMAKE_CXXFLAGS += -fsanitize=address
  #QMAKE_LFLAGS += -fsanitize=address
  LIBS += -fopenmp
}

msvc {
  QMAKE_CXXFLAGS += -openmp
}

INCLUDEPATH += \
    $$PWD/Headers

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    .gitignore \
    Demo/Demo.gif \
    README.md

RESOURCES += \
    resources.qrc

