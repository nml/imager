#ifndef SETUP_H
#define SETUP_H

//#include <cmath>
#include <QtMath>

#include <vector>
#include <stdexcept>

#include <QString>
#include <QSettings>
#include <QMap>
#include <QMetaType>
#include <QDir>
#include <QDebug>

class Vector3D
{
public:
    double X=0;
    double Y=0;
    double Z=0;
    Vector3D():Vector3D(0, 0, 0) {}
    Vector3D(const double x, const double y, const double z): X(x), Y(y), Z(z) {}

    Vector3D operator+(const Vector3D& r) const {
        return Vector3D(this->X + r.X, this->Y + r.Y, this->Z + r.Z);
    }
    Vector3D operator-(const Vector3D& r) const {
        return Vector3D(this->X - r.X, this->Y - r.Y, this->Z - r.Z);
    }

    double operator*(const Vector3D& r) const {
        return this->X * r.X + this->Y * r.Y + this->Z * r.Z;
    }
    Vector3D operator*(const double& d) const {
        return Vector3D(this->X * d,  this->Y * d, this->Z * d);
    }
    Vector3D operator/(const double& d) const {
        return Vector3D(this->X / d,  this->Y / d, this->Z / d);
    }
};

inline Vector3D operator*(const double& d, const Vector3D& r){
    return r * d;
}

inline double getCosAngle(const Vector3D& l, const Vector3D& r) {
    return l*r / std::sqrt((l*l) * (r*r));
}

struct Channel
{
    int id;
    bool enabled;
    double x;
    double y;
    int ASICID;
    int chNum;
    double z;
    QString source;
    double threshold;
    double caliCoef;
};

Q_DECLARE_METATYPE(Channel);

inline QDataStream& operator<<(QDataStream& out, const Channel& v) {
    out << QVariant(v.id).toString()
        << QVariant(v.enabled).toString() << QVariant(v.x).toString() << QVariant(v.y).toString()
        << QVariant(v.ASICID).toString()
        << QVariant(v.chNum).toString()
        << QVariant(v.z).toString()
        << QVariant(v.source).toString()
        << QVariant(v.threshold).toString()
        << QVariant(v.caliCoef).toString();
    return out;
}

inline QDebug operator<<(QDebug dbg, const Channel &v)
{
    QDebugStateSaver saver(dbg);
    dbg<< v.id << v.enabled << v.x << v.y <<
                     v.ASICID <<
                     v.chNum<<
                     v.z<<
                     v.source<<
                     v.threshold<<
                     v.caliCoef;
    return dbg;
}

inline QDataStream& operator>>(QDataStream& in, Channel& v) {
    QString s;
    in >> s;
    v.id = QVariant(s).toInt();
    in >> s;
    v.enabled = QVariant(s).toBool();
    in >> s;
    v.x = QVariant(s).toDouble();
    in >> s;
    v.y = QVariant(s).toDouble();
    in >> s;
    v.ASICID = QVariant(s).toInt();
    in >> s;
    v.chNum = QVariant(s).toInt();
    in >> s;
    v.z = QVariant(s).toDouble();
    in >> s;
    v.source = QVariant(s).toString();
    in >> s;
    v.threshold = QVariant(s).toDouble();
    in >> s;
    v.caliCoef = QVariant(s).toDouble();
    return in;
}

class Setup
{
private:
    /* data */
    QString settingPath;
public:
    Setup(const QString initFile);
    ~Setup();

    void readSetup(const QString path);
    void writeSetup(const QString path="settings.ini") const;
    // data file on disk
    QString filePath;
    QString inputFormat="BIN";
    QString runID = "0";
    QDir outputDir;
    // channels
    QList<Channel> channelSettings;
    bool allChannelOn=false;
    QString allSource="LG";
    double allThreshold=1.0;
    double allCaliCoef=1.0;

    // image space setup
    // radius of projection sphere,  mm
    double R;
    // Altitude angle, rad
    int thetaBins;
    double thetaMin;
    double thetaMax;
    // Azimuthal angle, rad
    int phiBins;
    double phiMin;
    double phiMax;

    // energy spectrum
    int ergBins;
    double ergMin;
    double ergMax;

    // LG/HG spectrum
    int LGHGErgBins=2048;
//    double LGHGErgMin=0;
//    double LGHGErgMax = 2048;

    // pair spectrum
    int pairErgBins=100;
    double pairErgMin=0;
    double pairErgMax=1000;

    // Rejections
    bool energyCut;
    double energyLow;
    double energyUp;

    // coincidence
    bool coincidenceEnabled;
    double timeWindow; // ns

    // data saving options
    bool saveImage=false;
    bool saveSpectrum=false;
    QString saveImageFormat = "TXT";
    QString saveSpectrumFormat="TXT";

    // FW use validation
    bool FWUseValidation = true;


    const double sgmE = 0.06; // 6% energy resolution
//    const Vector3D sgmpos = Vector3D(0.8 / 2, 0.43 / 2, 1.0 / 2); // mm
     const Vector3D sgmpos = Vector3D(0, 0, 0); // mm
    const int order=3;

    // refresh every 10 events
    const int chuckSize = 10;

    // max number of cones to process
    int maxN=100000;
};

class Cone
{
public:
    // interaction site 1 is the apex
    Vector3D apex;
    // site 1 - site 2 is the axis
    Vector3D axis;
    // half angle = abs (scattering angle)
    // for neutron, 0 < half angle < pi /2
    // for gamma,   0 < half angle < pi
    double cosHalfAngle;
    // initial energy
    double E0;
    // energy deposited in compoton scattering
    double Edpst;
    Cone(): Cone(Vector3D(0,0,0), Vector3D(0,0,0), 0, 0, 0) {}
    Cone(const Vector3D point, const Vector3D line, const double cosTheta, const double E0_, const double Edpst_):
       apex(point), axis(line), cosHalfAngle(cosTheta), E0(E0_), Edpst(Edpst_) {}
    Cone(const std::string& record) {
        apex = Vector3D(std::stod(record.substr(0, 8)),
                        std::stod(record.substr(8, 8)),
                        std::stod(record.substr(16, 8)));
        axis = Vector3D(std::stod(record.substr(24, 8)),
                        std::stod(record.substr(32, 8)),
                        std::stod(record.substr(40, 8)));
        cosHalfAngle = std::stod(record.substr(48, 13));
        E0 = std::stod(record.substr(61, 13));
        Edpst = std::stod(record.substr(74, 13));
    }
    Cone(const QString& record) {
        apex = Vector3D(record.mid(0, 8).toDouble(),
                        record.mid(8, 8).toDouble(),
                        record.mid(16, 8).toDouble());
        axis = Vector3D(record.mid(24, 8).toDouble(),
                        record.mid(32, 8).toDouble(),
                        record.mid(40, 8).toDouble());
        cosHalfAngle = record.mid(48, 13).toDouble();
        E0 = record.mid(61, 13).toDouble();
        Edpst = record.mid(74, 13).toDouble();
    }
};


class MyPulse
{
public:
    ulong histNo=0; // history number
    double timeStamp=0; // time stamp, ns
    double height=0; // pulse amplitude, MeV
    int cellNo=0; // cell number
    Vector3D pos; // position, cm
    MyPulse() {}
    MyPulse(std::string record) {
        timeStamp = std::stod(record.substr(0, 24));
        height = std::stod(record.substr(27, 10));
        pos = Vector3D(std::stod(record.substr(39, 6)),
                       std::stod(record.substr(47, 6)),
                       std::stod(record.substr(55, 6)));
        cellNo = std::stoi(record.substr(68, 6));
        histNo = std::stoi(record.substr(77, 10));
    }
    MyPulse(const QString record) {
        timeStamp = record.mid(0, 24).toDouble();
        height = record.mid(27, 10).toDouble();
        pos = Vector3D(record.mid(39, 6).toDouble(),
                       record.mid(47, 6).toDouble(),
                       record.mid(55, 6).toDouble());
        cellNo = record.mid(68, 6).toInt();
        histNo = record.mid(77, 10).toULong();
    }
};
#endif // SETUP_H
