#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QPointer>

#include "ui_MainWindow.h"
#include "formentry.h"
#include "plotwindow.h"
#include "plotdata.h"
#include "worker.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
//    virtual void changeEvent(QEvent *e);
    ~MainWindow();
    void closeEvent (QCloseEvent *event);
public slots:

private:
    Ui::MainWindow *ui;
    QPointer<QPlotWindow> plotwindow=nullptr;
    PlotData* plotdata=nullptr;
    Setup* config=nullptr;
    // worker thread responsible for image reconstruction
    QPointer<Worker> workerThread=nullptr;

    bool checkConfig();
    void showConfig();
    QList<FormEntry*> ChEntries;

    bool doubleEqual(const double& a, const double & b, const double epsilon=0.0001);
    void setupMapping();

    bool imageSpacechaged=false;
    std::atomic<bool> resultSaved;
protected:

private slots:
    void handleOpenplot();
    bool handleOpenProj();
    bool handleSave();
    bool handleSaveAs();
    void handleClose();
    void handleAbout();

    void handleStart();
    void handlePause();
    void handleStop();

    void notifyThreadFinished();
    void onConfigApplied();
    void onConfigChanged();

    void onOpenFileClicked();
    void oninputFormatChanged(const QString text);
    void onRunIDInput();
    void onMaxEventNumInput();

    void onSaveImageStateChanged();
    void onSaveSpectrumStateChanged();
    void onsaveImageFormatChanged(const QString text);
    void onsaveSpectrumFormatChanged(const QString text);

    void onRadiusInput();
    void onPhiBinsInput();
    void onPhiMinInput();
    void onPhiMaxInput();
    void onThetaBinsInput();
    void onThetaMinInput();
    void onThetaMaxInput();

    void onErgBinsInput();
    void onErgMinInput();
    void onErgMaxInput();

    void onErgCutStateChanged();
    void onErgCutLowInput();
    void onErgCutHighInput();

    void onPairErgBinsInput();
    void onPairErgMinInput();
    void onPairErgMaxInput();

    void onLGHGBinBox(const QString text);

    void onCoinOnStatChanged();
    void onTimeWindowInput();

    void onStatChanged(int id);
    void onPosXChanged(int id);
    void onPosYChanged(int id);
    void onPosZChanged(int id, int entry);
    void onASICIDChanged(int id, int entry);
    void onchNumChanged(int id, int entry);
    void onSourceChanged(int id, int entry);
    void onThreChanged(int id, int entry);
    void onCaliCoefChanged(int id, int entry);

    void onAllStatChanged();
    void onAllSourceChanged();
    void onAllThreChanged();
    void onAllCaliCoefChanged();

signals:
    void workerStarted();
//    void configChanged();
};

#endif // MAINWINDOW_H
