#ifndef PLOTWINDOW_H
#define PLOTWINDOW_H


#include <QMainWindow>
#include "ui_plotwindow.h"
#include "qcustomcanvas.h"
#include "plotdata.h"
#include "worker.h"

namespace Ui {
class PlotWindow;
}

class QPlotWindow : public QMainWindow
{
    Q_OBJECT
private:
    const Setup* config;
    Ui::PlotWindow *ui;

    QCPColorMap *image=nullptr;

    void setupImage(QCustomPlot* customPlot);
    void updateImage(QCustomPlot* customPlot);

    void setupCoinSpectrum(QCustomPlot* customPlot);
    void setupLGHGSpectrum(QCustomPlot* customPlot);
    void setupPairSpectrum(QCustomPlot* customPlot);
    void updateCoinSpectrum(QCustomPlot* customPlot);
    void updatePairSpectrum(QCustomPlot* customPlot);
    void updateLGHGSpectrum(QCustomPlot* customPlot);

    int LGAsicID = 0;
    int LGChannelID = 0;
    int HGAsicID = 0;
    int HGChannelID = 0;
    int pairID = 0;
    QString source="LG";

    void switchLGHG(int id);
    void switchPair(int id);

public:
     QPlotWindow(QWidget *parent, const Setup* config_, PlotData* plotdata_);
    ~QPlotWindow();

    PlotData* plotdata;
    void redraw();
    void reopen();
    void rebinImage();
signals:
    void runStarted();
    void runPaused();
    void runStopped();

public slots:
    void handleStart();
    void handlePause();
    void handleStop();

    void handleUnzoom(int id=0);
    void handleClear(int id=0);
    void handleLogscale(int id=0);
    void handleScreenshot(int id=0);
    void handleSave2txt(int id=0);

    void handleAsicChanged(int id);
    void handleChannelChanged(int id);

    void handleSourceChanged(int id);
    void handlePairChanged(int id);
};

#endif // PLOTWINDOW_H
