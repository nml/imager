#ifndef FORMENTRY_H
#define FORMENTRY_H

#include <QWidget>
#include "ui_formEntry.h"

namespace Ui {
class FormEntry;
}

class FormEntry : public QWidget
{
    Q_OBJECT
public:
    explicit FormEntry(QWidget *parent = nullptr);

    // getters
    int getId();
    bool getStat();
    double getPosX();
    double getPosY();
    double getPosZ(int entry);
    int getASICID(int entry);
    int getChNum(int entry);
    QString getSource(int entry);
    double getThreshold(int entry);
    double getCaliCoef(int entry);

    // setters
    // entry = 1 <=> first entry
    // entry = 2 <=> second entry
    void setId(const int id);
    void setStat(const bool stat);
    void setPosX(const double x);
    void setPosY(const double y);
    void setPosZ(const double z, const int entry);
    void setASICID(const int asicid, const int entry);
    void setChNum(const int chnum, const int entry);
    void setSource(const QString src, const int entry);
    void setThreshold(const double thre, int entry);
    void setCaliCoef(const double calicoef, int entry);

private:
    Ui::FormEntry* ui;
    int UID;
signals:

    void statChanged(int id);
    void posXChnaged(int id);
    void posYChanged(int id);
    void posZChanged(int id, int entry);
    void ASICIDChanged(int id, int entry);
    void chNumChanged(int id, int entry);
    void sourceChanged(int id, int entry);
    void thresholdCHanged(int id, int entry);
    void caliCoefChanged(int id, int entry);

public slots:
    void onStatChanged();
    void onPosXChanged();
    void onPosYChanged();
    void onPosZChanged();
    void onASICIDChanged();
    void onchNumChanged();
    void onSourceChanged();
    void onThreChanged();
    void onCaliCoefChanged();

};

#endif // FORMENTRY_H
