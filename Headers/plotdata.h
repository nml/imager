#ifndef PLOTDATA_H
#define PLOTDATA_H

#include <QtMath>
#include <iomanip>

#include <mutex>
#include "setup.h"

class Histogram
{
public:
    Histogram() {}
    Histogram(const int nbins_, const double lower_, const double upper_)
        : nbins(nbins_),
          lowerEdge(lower_),
          upperEdge(upper_)
    {
        rebin(nbins, lowerEdge, upperEdge);
    }

    // getters
    int getNBins() const
    {
        return nbins;
    }
    double getBinContent(const int& binIndex) const
    {
        return binCounts[binIndex];
    }
    double getBinCenter(const int& binIndex) const
    {
        return binCenters[binIndex];
    }
    QVector<double> getBinCenters() const
    {
        return binCenters;
    }
    QVector<double> getBinContents() const
    {
        return binCounts;
    }
    int getTotalCounts() const
    {
        return totalCounts;
    }

    // fill new data
    bool fill(const double item)
    {
        if (item >= upperEdge || item < lowerEdge)
        {
            return false;
        }
        int binIndex = (item - lowerEdge) / binwidth;
//        if(binIndex < 0 || binIndex >= nbins)
//            return false;
        binCounts[binIndex] += 1;
        totalCounts += 1;
        return true;
    }

    // clear
    void clear()
    {
        totalCounts = 0;
        std::fill(binCounts.begin(), binCounts.end(), 0);
    }

    // rebin
    void rebin(const int nbins_, const double lower_, const double upper_)
    {
        totalCounts=0;
        nbins = nbins_;
        lowerEdge = lower_;
        upperEdge = upper_;
        binCounts = QVector<double>(nbins, 0);
        binCenters = QVector<double>(nbins, 0);
        binwidth = (upperEdge-lowerEdge) / nbins;
        binCenters[0] = lowerEdge + 0.5 * binwidth;
        for (int i=1;i<nbins;i++)
        {
            binCenters[i] = binCenters[i-1]+binwidth;
        }
    }

private:
    int nbins;
    double lowerEdge;
    double upperEdge;
    double binwidth;
    QVector<double> binCenters;
    QVector<double> binCounts;
    int totalCounts;
};

class PlotData
{
public:
    PlotData(const Setup* config_);
    std::mutex mMutex;

    // getters
    int getImageCounts();

    const std::vector<std::vector<double>>& getImageData();

    int getCoinTotalCounts();

    QVector<double> getCoinBinCenters();

    QVector<double> getCoinBinCounts();


    int getPairTotalCounts(const int& pair);

    QVector<double> getPairBinCenters(const int& pair);

    QVector<double> getPairBinCounts(const int& pair);
    int getLGTotalCounts(const int& asic, const int& channel);

    QVector<double> getLGBinCenters(const int& asic, const int& channel);

    QVector<double> getLGBinCounts(const int& asic, const int& channel);

    int getHGTotalCounts(const int& asic, const int& channel);

    QVector<double> getHGBinCenters(const int& asic, const int& channel);

    QVector<double> getHGBinCounts(const int& asic, const int& channel);

    void saveOnExit();
    void clearAll();
    void clearImage();
    void rebinImage();
    void rebinSpectrum();
    void clearSpectrum();
    bool saveImage2txt(const std::string& fileName);
    bool saveSpectrum2txt(const std::string& fileName);

    void clearPairSpectra();
    bool savePairSpectrum2txt(const std::string&fileName, const int& pair);
    void rebinPairSpectra();
    void updatePairSpectra(const std::vector<MyPulse>& pulses);

    void clearLGSpectrum(const int& asic, const int& channel);
    void clearHGSpectrum(const int& asic, const int& channel);
    bool saveLGSpectrum2txt(const std::string&fileName, const int& asic, const int& channel);
    bool saveHGSpectrum2txt(const std::string&fileName, const int& asic, const int& channel);
    void clearLGHG();
    void rebinLGSpectra();
    void rebinHGSpectra();

    void updateLGSpectra(const int asic, const ushort (&chargeLG)[32]);
    void updateHGSpectra(const int asic, const ushort (&chargeHG)[32]);

    /**
     * @brief add cones to current image and update.
     *
     * @param first Input. First iterator to the vector of cones.
     * @param last Input. Last iterator to the vector of cones.
     * @param normailzed Input. Normalize the image if true.
     */
    void updateImage(std::vector<Cone>::const_iterator first,
                     std::vector<Cone>::const_iterator last,
                     const bool normailzed=true);
private:
    const Setup* config; // Global settings.
    uint64_t counts=0; // total number of cones (events)
    std::vector<std::vector<double>> imageData;
    double dtheta;
    double dphi;
    std::vector<double> thetaBinCenters;
    std::vector<double> phiBinCenters;
    // pixelate the projection sphere
    std::vector<std::vector<Vector3D>> xbs;
    int pixelateSphere();
    Histogram coinSpectrum;
    bool spectrum2txt(const Histogram& spectrum, const std::string& fileName);
    std::vector<std::vector<double>> probDist;

    // Pair spectra
    std::vector<Histogram> pairSpectra;
    // LG/HG
    std::vector<Histogram> LGSpectra;
    std::vector<Histogram> HGSpectra;
    /**
     * @brief Project cones onto the sphereical surface and update the image.
     *        Implementation based on
     *        https://www.nature.com/articles/s41598-020-58857-z
     *
     * @param first Input. First iterator to the vector of cones.
     * @param last Input. Last iterator to the vector of cones.
     * @return 0
     */
    int addCones(std::vector<Cone>::const_iterator first,
                 std::vector<Cone>::const_iterator last);

    /**
     * @brief Project cones onto the sphereical surface and update the image.
     *        Imeplemention based on
     *        https://www.overleaf.com/read/hjdvrcrjcvpx
     *
     * @param first Input. First iterator of the vector of cones.
     * @param last Input. Last iterator of the vector of cones.
     * @return 0
     */
    int addConesNormalized(std::vector<Cone>::const_iterator first,
                           std::vector<Cone>::const_iterator last);
};
#endif // PLOTDATA_H
