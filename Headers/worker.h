#ifndef WORKER_H
#define WORKER_H

#include <fstream>
#include <atomic>
#include <QObject>
#include <QThread>
#include <QTextStream>
#include "plotdata.h"

class Event
{
public:
    Event() {}

    int id;
    uint64_t EventCounter;
    int AsicID;
    int channelNum;
    double RunEventTimecode_ns;
    double energyLG;
    double energyHG;
};

class DataCITIROC
{
public:
    DataCITIROC() {}
    DataCITIROC(QVector<QStringRef>::ConstIterator iter)
    {
        AsicID = (iter++)->toInt();
        EventCounter = (iter++)->toInt();
        RunEventTimecode = (iter++)->toInt();
        RunEventTimecode_ns = (iter++)->toDouble();
        EventTimecode = (iter++)->toInt();
        EventTimecode_ns = (iter++)->toDouble();
        for (int i=0;i<32;i++)
        {
            hit[i] = (iter++)->toInt();
        }
        for (int i=0;i<32;i++)
        {
            chargeLG[i] = (iter++)->toInt();
        }
        for (int i=0;i<32;i++)
        {
            chargeHG[i] = (iter++)->toInt();
        }
    }
    uint64_t EventTimecode;
    uint64_t RunEventTimecode;
    uint64_t EventCounter;
    uint16_t AsicID;

    double EventTimecode_ns;
    double RunEventTimecode_ns;

    ushort chargeLG[32];
    ushort chargeHG[32];
    bool   hit[32];
};

class EnabledChannel
{
private:
    // ASIC ID, Ch num
    QMap<QPair<int, int>, int> CHIDmap;
public:
    EnabledChannel() {}
    EnabledChannel(const QList<Channel> chset)
    {
        for (int i=0;i<chset.size();i++)
        {
            if(chset[i].enabled){
                CHIDmap[qMakePair(chset[i].ASICID, chset[i].chNum)] = i;
            }
        }
    }

    // getter
    int getID(const QPair<int, int> ch) const
    {
        if (CHIDmap.contains(ch))
            return CHIDmap[ch];
        else
            return -1;
    }
    int getPairID(const QPair<int, int> ch) const
    {
        int id = getID(ch);
        if (id < 0)
            return -1;
        return 4 * (id / 2) + 1 - id;
    }
};

class Worker : public QThread
{
    Q_OBJECT
private:
    const Setup* config;
    PlotData* plotdata;
    void run() override;
    std::atomic<bool> stopped;
    std::atomic<bool> exitted;

    QFile infile;
    QFile outfile;

    EnabledChannel enabledChs;

    void processPackets(const std::vector<DataCITIROC>& packets, std::vector<Cone> &cones);
    bool processCoincidence(const std::vector<MyPulse>& pulses, Cone& newCone);
    void saveCones(const std::vector<Cone>& cones, QTextStream& out);
    int eventSize = 152; // 152 bytes
    void readBIN(std::vector<Cone>& cones, QDataStream& in);
    void readCSVFULL(std::vector<Cone> &cones, QTextStream &in);
    void readCSVDECODEEVENTS(std::vector<Cone> &cones, QTextStream &in);
    int DecodeCITIROCRowEvents(const char* buf, const int bufSize, DataCITIROC& DataCITIROCA);
    bool extractEvents(const std::vector<DataCITIROC>& packets, std::vector<Event>& events);
    bool event2Pulses(const std::vector<Event>& events, std::vector<MyPulse>& pulses);
    double findZ(const double & E1, const double & E2, const double & Z1, const double & Z2);

    // hard-coded cali coefs
    std::vector<std::vector<double>> calibrationCoeffs;
public:
//    explicit Worker(QObject *parent = nullptr);
    Worker(QObject *parent, const Setup* config_, PlotData* plotdata_);

//signals:

public slots:
    void handleStart();
    void handleStop();
    void handlePause();
};

#endif // WORKER_H
