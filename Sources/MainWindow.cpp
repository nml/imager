#include "MainWindow.h"

#include <QVBoxLayout>
#include <QTimer>
#include <QDebug>
#include <QWindow>
#include <QFileDialog>
#include <QDateTime>
#include <QMessageBox>
#include <QDoubleValidator>
#include <QIntValidator>
#include <QRegExpValidator>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // General
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::handleOpenProj);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::handleClose);
    connect(ui->actionSave, &QAction::triggered, this, &MainWindow::handleSave);
    connect(ui->actionSave_As, &QAction::triggered, this, &MainWindow::handleSaveAs);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::handleAbout);
    connect(ui->actionOpenPlot, &QAction::triggered, this, &MainWindow::handleOpenplot);


    connect(ui->actionStart, &QAction::triggered, this, &MainWindow::handleStart);
    connect(ui->actionPause, &QAction::triggered, this, &MainWindow::handlePause);
    connect(ui->actionStop, &QAction::triggered, this, &MainWindow::handleStop);

    // accepts user input and update config
    // quit worker thread and clear plot data immediately when any settings are changed
    connect(ui->openProject, &QPushButton::clicked, this, &MainWindow::handleOpenProj);
    connect(ui->openFileButton, &QPushButton::clicked, this, &MainWindow::onOpenFileClicked);
    connect(ui->inputFormat, &QComboBox::currentTextChanged, this, &MainWindow::oninputFormatChanged);
    ui->runIDInput->setValidator(new QRegExpValidator( QRegExp("[A-Za-z0-9_]+"), ui->runIDInput));
    connect(ui->runIDInput, &QLineEdit::editingFinished, this, &MainWindow::onRunIDInput);
    ui->maxEventNumInput->setValidator(new QIntValidator(1, 100000000,ui->maxEventNumInput));
    connect(ui->maxEventNumInput, &QLineEdit::editingFinished, this, &MainWindow::onMaxEventNumInput);

    // Plot
    QDoubleValidator* rdv = new QDoubleValidator(1, 1000000, 6, ui->radiusInput);
    rdv->setNotation(QDoubleValidator::StandardNotation);
    ui->radiusInput->setValidator(rdv);
    connect(ui->radiusInput, &QLineEdit::editingFinished, this, &MainWindow::onRadiusInput);

    ui->phiBinsInput->setValidator(new QIntValidator(1, 1000000, ui->phiBinsInput));
    QDoubleValidator* phimindv = new QDoubleValidator(-360, 360, 6, ui->phiMinInput);
    phimindv->setNotation(QDoubleValidator::StandardNotation);
    ui->phiMinInput->setValidator(phimindv);
    ui->phiMaxInput->setValidator(phimindv);
    connect(ui->phiBinsInput, &QLineEdit::editingFinished, this, &MainWindow::onPhiBinsInput);
    connect(ui->phiMinInput, &QLineEdit::editingFinished, this, &MainWindow::onPhiMinInput);
    connect(ui->phiMaxInput, &QLineEdit::editingFinished, this, &MainWindow::onPhiMaxInput);

    ui->thetaBinsInput->setValidator(new QIntValidator(1, 1000000, ui->thetaBinsInput));
    QDoubleValidator* thetamindv = new QDoubleValidator(-180, 180, 6, ui->thetaMinInput);
    thetamindv->setNotation(QDoubleValidator::StandardNotation);
    ui->thetaMinInput->setValidator(thetamindv);
    ui->thetaMaxInput->setValidator(thetamindv);
    connect(ui->thetaBinsInput, &QLineEdit::editingFinished, this, &MainWindow::onThetaBinsInput);
    connect(ui->thetaMinInput, &QLineEdit::editingFinished, this, &MainWindow::onThetaMinInput);
    connect(ui->thetaMaxInput, &QLineEdit::editingFinished, this, &MainWindow::onThetaMaxInput);

    ui->ergBinsInput->setValidator(new QIntValidator(1, 1000000, ui->ergBinsInput));
    QDoubleValidator* ergmindv = new QDoubleValidator(0, 1000000, 6, ui->ergMinInput);
    ergmindv->setNotation(QDoubleValidator::StandardNotation);
    ui->ergMinInput->setValidator(ergmindv);
    ui->ergMaxInput->setValidator(ergmindv);
    connect(ui->ergBinsInput, &QLineEdit::editingFinished, this, &MainWindow::onErgBinsInput);
    connect(ui->ergMinInput, &QLineEdit::editingFinished, this, &MainWindow::onErgMinInput);
    connect(ui->ergMaxInput, &QLineEdit::editingFinished, this, &MainWindow::onErgMaxInput);

    QDoubleValidator* ergcutdv = new QDoubleValidator(0, 1000000, 6, ui->ergCutLowInput);
    ergcutdv->setNotation(QDoubleValidator::StandardNotation);
    ui->ergCutLowInput->setValidator(ergcutdv);
    ui->ergCutHighInput->setValidator(ergcutdv);
    connect(ui->enableErgCutBox, &QCheckBox::stateChanged, this, &MainWindow::onErgCutStateChanged);
    connect(ui->ergCutLowInput, &QLineEdit::editingFinished, this, &MainWindow::onErgCutLowInput);
    connect(ui->ergCutHighInput, &QLineEdit::editingFinished, this, &MainWindow::onErgCutHighInput);

    connect(ui->saveImageBox, &QCheckBox::stateChanged, this, &MainWindow::onSaveImageStateChanged);
    connect(ui->saveSpectrumBox, &QCheckBox::stateChanged, this, &MainWindow::onSaveSpectrumStateChanged);
    connect(ui->saveImageFormat, &QComboBox::currentTextChanged, this, &MainWindow::onsaveImageFormatChanged);
    connect(ui->saveSpectrumFormat, &QComboBox::currentTextChanged, this, &MainWindow::onsaveSpectrumFormatChanged);

    QDoubleValidator* tmedv = new QDoubleValidator(0, 1000000, 6, ui->timeWindowInput);
    tmedv->setNotation(QDoubleValidator::StandardNotation);
    ui->timeWindowInput->setValidator(tmedv);
    connect(ui->timeWindowInput, &QLineEdit::editingFinished, this, &MainWindow::onTimeWindowInput);
    connect(ui->enableCoincidencebox, &QCheckBox::stateChanged, this, &MainWindow::onCoinOnStatChanged);

    ui->pairErgBinInput->setValidator(new QIntValidator(1, 1000000, ui->pairErgBinInput));
    QDoubleValidator* pairergmindv = new QDoubleValidator(0, 1000000, 6, ui->pairErgMinInput);
    pairergmindv->setNotation(QDoubleValidator::StandardNotation);
    ui->pairErgMinInput->setValidator(pairergmindv);
    ui->pairErgMaxInput->setValidator(pairergmindv);
    connect(ui->pairErgBinInput, &QLineEdit::editingFinished, this, &MainWindow::onPairErgBinsInput);
    connect(ui->pairErgMinInput, &QLineEdit::editingFinished, this, &MainWindow::onPairErgMinInput);
    connect(ui->pairErgMaxInput, &QLineEdit::editingFinished, this, &MainWindow::onPairErgMaxInput);

    connect(ui->LGHGBinBox, &QComboBox::currentTextChanged, this, &MainWindow::onLGHGBinBox);

    // Mapping
    setupMapping();

    resultSaved=false;

    setWindowTitle(tr("Back projection"));

    qDebug() << "Attempting to open a project";
    if(!handleOpenProj())
    {
        qDebug() << "Cannot open project. Exit application.";
//        QCoreApplication::exit(1);
        exit(EXIT_FAILURE);
    }
    ui->statusBar->showMessage("Ready.");
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Close",
                                                               tr("Are you sure you want to quit?\n"),
                                                               QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                               QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        if (workerThread) {
            // stop worker thread
            ui->actionStop->trigger();
            qDebug() << "Writing results to disk";
            workerThread->wait();
            qDebug() << "Result saving done";
        }
        event->accept();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    if (config){
        qDebug() << "write settings on main window closed";
        config->writeSetup(config->outputDir.absolutePath() + "/settings.ini");
        delete config;
        qDebug() << "settings saved";
    }
    if (plotdata)
    {
        delete plotdata;
    }
}

void MainWindow::handleOpenplot()
{
    if(!plotwindow.isNull() && plotwindow->isVisible())
    {
        // bring to front
        plotwindow->raise();
        return;
    }
    qDebug() << "Open new plot window";
    if(!plotdata)
    {
        QMessageBox::critical(this, "Warning", "Apply the settings first by pressing start button.");
        return;
    }
    plotwindow = new QPlotWindow(this, config, plotdata);
    connect(plotwindow, &QPlotWindow::runStarted, ui->actionStart, &QAction::trigger);
    connect(plotwindow, &QPlotWindow::runPaused, ui->actionPause, &QAction::trigger);
    connect(plotwindow,&QPlotWindow::runStopped, ui->actionStop, &QAction::trigger);

    plotwindow->setWindowTitle("Plots");
    plotwindow->setAttribute(Qt::WA_DeleteOnClose);
    plotwindow->resize(plotwindow->sizeHint());
    plotwindow->show();
    plotwindow->showMaximized();
}

bool MainWindow::handleOpenProj()
{
    qDebug() << "Open project clicked";
    if (config)
    {
        // save current settings
        config->writeSetup("settings.ini");
        // stop current run
        onConfigChanged();
    }
    // select project dir
    QFileDialog dialog;
    dialog.setWindowTitle("Open a new or existing project");
    dialog.setFileMode(QFileDialog::DirectoryOnly);
    dialog.setOption(QFileDialog::ShowDirsOnly, false);
    if(!dialog.exec())
        return false;

    QFileInfo proj_dir(dialog.directory().absolutePath());
    if (!proj_dir.isDir())
    {
        QMessageBox::critical(this, "Failed to open direcory", QString("%1 is not a directory.").arg(proj_dir.absolutePath()));
        return false;
    }
    if (!proj_dir.isWritable())
    {
        QMessageBox::critical(this, "Permission denied", QString("%1 is not writable.").arg(proj_dir.absolutePath()));
        return false;
    }
    //    qDebug() << QDir::current();

    // if settings.ini does not exist, copy the template settings.ini file to current dir
    QString settingsPath = dialog.directory().absolutePath() + "/settings.ini";
    if (!dialog.directory().exists("settings.ini"))
    {
        if(!QFile::copy(":/Data/Data/settings.ini", settingsPath)){
            QMessageBox::critical(this, "Cannot create file", QString("Cannot create settings.ini in %1").arg(proj_dir.absolutePath()));
            return false;
        }
    }
    // check read / write permission
    if(!QFileInfo(settingsPath).isReadable() || !QFileInfo(settingsPath).isWritable())
    {
        if (!QFile::setPermissions(settingsPath, QFileDevice::ReadOwner | QFileDevice::WriteOwner))
        {
            QMessageBox::critical(this, "Permission denied", QString("Cannot read or write settings.ini in %1").arg(proj_dir.absolutePath()));
            return false;
        }
    }

    // set current path to this dir
    QDir::setCurrent(dialog.directory().absolutePath());
    // read .ini file in current dir
    if (!config) // start up
        config = new Setup("settings.ini");
    else {
        config->readSetup("settings.ini");
    }
    showConfig();
    return true;
}

bool MainWindow::handleSave()
{
    qDebug() << "Save clicked";
    if (config){
        config->writeSetup("settings.ini");
        ui->statusBar->showMessage("Project settings saved.");
        return true;
    }
    return false;
}

bool MainWindow::handleSaveAs()
{
    qDebug() << "Save as clicked";
    if (!config)
        return false;

    // save current settings
    config->writeSetup("settings.ini");
    // stop current run
    onConfigChanged();
    // select new dir
    QFileDialog dialog;
    dialog.setWindowTitle("Save as");
    dialog.setFileMode(QFileDialog::DirectoryOnly);
    dialog.setOption(QFileDialog::ShowDirsOnly, false);
    if(!dialog.exec())
        return false;

    QFileInfo proj_dir(dialog.directory().absolutePath());
    if (!proj_dir.isDir())
    {
        QMessageBox::critical(this, "Failed to open direcory", QString("%1 is not a directory.").arg(proj_dir.absolutePath()));
        return false;
    }
    if (!proj_dir.isWritable())
    {
        QMessageBox::critical(this, "Permission denied", QString("%1 is not writable.").arg(proj_dir.absolutePath()));
        return false;
    }
    //    qDebug() << QDir::current();

    // if settings.ini does not exist, copy the template settings.ini file to current dir
    QString settingsPath = dialog.directory().absolutePath() + "/settings.ini";
    if (!dialog.directory().exists("settings.ini"))
    {
        if(!QFile::copy("settings.ini", settingsPath)){
            QMessageBox::critical(this, "Cannot create file", QString("Cannot create settings.ini in %1").arg(proj_dir.absolutePath()));
            return false;
        }
    }
    // check read / write permission
    if(!QFileInfo(settingsPath).isReadable() || !QFileInfo(settingsPath).isWritable())
    {
        if (!QFile::setPermissions(settingsPath, QFileDevice::ReadOwner | QFileDevice::WriteOwner))
        {
            QMessageBox::critical(this, "Permission denied", QString("Cannot read or write settings.ini in %1").arg(proj_dir.absolutePath()));
            return false;
        }
    }

    // set current path to this dir
    QDir::setCurrent(dialog.directory().absolutePath());
    // read .ini file in current dir
    config->readSetup("settings.ini");
    showConfig();
    ui->statusBar->showMessage(QString("Project saved to %1").arg(QDir::currentPath()));
    return true;
}

void MainWindow::handleClose()
{
    qDebug() << "Close clicked";
    this->close();
}

void MainWindow::handleAbout()
{
    QMessageBox::about(this, tr("About Application"),
             tr("This application is created using Qt 5.13.2. Source code is available at Gitlab."));
//    qDebug() << "About clicked";
}

void MainWindow::notifyThreadFinished()
{
    resultSaved=true;
    ui->statusBar->showMessage("Stopped");
    // pop up a message box indicating processing is finished.
//    if (!aborted)
//    {
//        QMessageBox messageBox;
//        messageBox.setText("Image reconstruction finished.");
//        messageBox.setWindowTitle("Finished");
//        messageBox.exec();
//    }
}

void MainWindow::handleStart()
{
    // if config is changed, or user pressed Stop button,
    // workerThread must be null.
    // Apply the new settings and start a new run
    // If it's not null,
    // then resume current run
    if (!workerThread.isNull())
    {
        emit workerStarted();
        ui->statusBar->showMessage("Running");
//        // save current setting
//        config->writeSetup(config->outputDir.absolutePath() + "/settings.ini");
//        qDebug() << "Write settings to " << config->outputDir.absolutePath();
    }
    else{
        onConfigApplied();
    }
}

void MainWindow::handlePause()
{
    if (!workerThread.isNull())
        ui->statusBar->showMessage("Paused.");
//    // save current setting
//    config->writeSetup(config->outputDir.absolutePath() + "/settings.ini");
//    qDebug() << "Write settings to " << config->outputDir.absolutePath();
}

void MainWindow::handleStop()
{
    if (!workerThread.isNull())
        ui->statusBar->showMessage("Stopped.");
    // save current setting
    config->writeSetup(config->outputDir.absolutePath() + "/settings.ini");
    qDebug() << "Write settings to " << config->outputDir.absolutePath();
}

void MainWindow::showConfig()
{
    // display settings in settings.ini file
    ui->projPathLabel->setText(QDir::currentPath());
    ui->filePathLabel->setText(config->filePath);
    ui->runIDInput->setText(config->runID);
    ui->maxEventNumInput->setText(QString::number(config->maxN));
    ui->inputFormat->setCurrentText(config->inputFormat);

    ui->saveImageBox->setChecked(config->saveImage);
    ui->saveSpectrumBox->setChecked(config->saveSpectrum);
    ui->saveImageFormat->setCurrentText(config->saveImageFormat);
    ui->saveSpectrumFormat->setCurrentText(config->saveSpectrumFormat);

    ui->radiusInput->setText(QString::number(config->R, 'f', 4));
    ui->phiBinsInput->setText(QString::number(config->phiBins));
    ui->phiMinInput->setText(QString::number(config->phiMin * 180 / M_PI, 'f', 4));
    ui->phiMaxInput->setText(QString::number(config->phiMax * 180 / M_PI, 'f', 4));
    ui->thetaBinsInput->setText(QString::number(config->thetaBins));
    ui->thetaMinInput->setText(QString::number(config->thetaMin * 180 / M_PI, 'f', 4));
    ui->thetaMaxInput->setText(QString::number(config->thetaMax * 180 / M_PI, 'f', 4));

    ui->ergBinsInput->setText(QString::number(config->ergBins));
    ui->ergMinInput->setText(QString::number(config->ergMin, 'f', 4));
    ui->ergMaxInput->setText(QString::number(config->ergMax, 'f', 4));

    ui->enableErgCutBox->setChecked(config->energyCut);
    ui->ergCutLowInput->setText(QString::number(config->energyLow, 'f', 4));
    ui->ergCutHighInput->setText(QString::number(config->energyUp, 'f', 4));

    ui->pairErgBinInput->setText(QString::number(config->pairErgBins));
    ui->pairErgMinInput->setText(QString::number(config->pairErgMin, 'f', 4));
    ui->pairErgMaxInput->setText(QString::number(config->pairErgMax, 'f', 4));

    ui->LGHGBinBox->setCurrentText(QString::number(config->LGHGErgBins));

    ui->enableCoincidencebox->setChecked(config->coincidenceEnabled);
    ui->timeWindowInput->setText(QString::number(config->timeWindow, 'f', 4));

    // mapping
    ui->AllChannelOnBox->setChecked(config->allChannelOn);
    ui->AllSourceBox->setCurrentText(config->allSource);
    ui->AllThreInput->setText(QString::number(config->allThreshold, 'f', 4));
    ui->AllCaliCoefInput->setText(QString::number(config->allCaliCoef, 'f', 4));
    for (int i=0; i<config->channelSettings.size() / 2 && i< ChEntries.size(); i++) {
        ChEntries[i]->setId(i);
        ChEntries[i]->setStat(config->channelSettings[2*i].enabled);
        ChEntries[i]->setPosX(config->channelSettings[2*i].x);
        ChEntries[i]->setPosY(config->channelSettings[2*i].y);

        for (int j = 0; j < 2; j++) {
            ChEntries[i]->setASICID(config->channelSettings[2*i+j].ASICID, j+1);
        }
        for (int j = 0; j < 2; j++) {
            ChEntries[i]->setChNum(config->channelSettings[2*i+j].chNum, j+1);
        }
        for (int j = 0; j < 2; j++) {
            ChEntries[i]->setPosZ(config->channelSettings[2*i + j].z, j+1);
        }
        for (int j = 0; j < 2; j++) {
            ChEntries[i]->setSource(config->channelSettings[2*i+j].source, j+1);
        }
        for (int j = 0; j < 2; j++) {
            ChEntries[i]->setThreshold(config->channelSettings[2*i+j].threshold, j+1);
        }
        for (int j = 0; j < 2; j++) {
            ChEntries[i]->setCaliCoef(config->channelSettings[2*i+j].caliCoef, j+1);
        }
    }

}


bool MainWindow::checkConfig()
{
    // check if file exists
    if (!QFile(config->filePath).exists()){
        QMessageBox::critical(this, "File not exist", QString("Cannot open file %1").arg(config->filePath));
        return false;
    }
    // check if runID already exists
    if (config->outputDir.exists()) {
        // check if it's writable
        if (!QFileInfo(config->outputDir.absolutePath()).isWritable())
        {
            QMessageBox::critical(this, "Permission denied", QString("Cannot write to directory %1").arg(config->outputDir.absolutePath()));
            return false;
        }
        QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Overwrite",
                                                                   QString("Run-%1 already exists. Are you sure you want to overwrite?\n").arg(config->runID),
                                                                   QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                                   QMessageBox::Yes);
        if (resBtn != QMessageBox::Yes) {
            return false;
        }
    }
    else {
        // create a new dir for current ID
       if(!config->outputDir.mkpath(".")){
           QMessageBox::critical(this, "Permission denied", "Cannot create output directory for run "+config->runID);
           return false;
       }
       QString outdirpath = config->outputDir.absolutePath();
       // check read / write permission
       if(!QFileInfo(outdirpath).isReadable() || !QFileInfo(outdirpath).isWritable())
       {
           if (!QFile::setPermissions(outdirpath, QFileDevice::ReadOwner | QFileDevice::WriteOwner))
           {
               QMessageBox::critical(this, "Permission denied", QString("Cannot read or write to directoy %1").arg(outdirpath));
               return false;
           }
       }
    }

    if (config->coincidenceEnabled)
    {
        if (config->phiMax <= config->phiMin)
        {
            QMessageBox::warning(this, "Warning", QString("Azimuthal angle: Min phi must be smaller than max phi."));
            return false;
        }
        if (config->phiMax - config->phiMin > 360)
        {
            QMessageBox::warning(this, "Warning", QString("Azimuthal angle: Max phi - min phi must not be greater than 360."));
            return false;
        }
        if (config->thetaMax <= config->thetaMin)
        {
            QMessageBox::warning(this, "Warning", QString("Elvation angle: Min theta must be smaller than max theta."));
            return false;
        }
        if (config->thetaMax - config->thetaMin > 180)
        {
            QMessageBox::warning(this, "Warning", QString("Elvation angle: Max theta - min theta must not be greater than 180."));
            return false;
        }
        if (config->ergMax <= config->ergMin)
        {
            QMessageBox::warning(this, "Warning", QString("Coincidence Energy Spectrum: Min energy must be smaller than max energy."));
            return false;
        }
    }
    if (config->energyCut && config->energyUp <= config->energyLow)
    {
        QMessageBox::warning(this, "Warning", QString("Energy Cut: Energy low cut must be smaller than energy high cut."));
        return false;
    }

    if (config->pairErgMax <= config->pairErgMin)
    {
        QMessageBox::warning(this, "Warning", QString("Pair Energy Spectrum: Min energy must be smaller than max energy."));
        return false;
    }

    // check for duplicates
    // lazy nested loop
    for (int i=0; i<config->channelSettings.size(); i++) {
        if (!config->channelSettings[i].enabled)
            continue;
        for (int j=i+1; j<config->channelSettings.size(); j++) {
            if (!config->channelSettings[j].enabled)
                continue;
            if (config->channelSettings[j].ASICID == config->channelSettings[i].ASICID &&
                config->channelSettings[j].chNum == config->channelSettings[i].chNum){
                QMessageBox::warning(this, "Duplicated Channel.",
                                     QString("ID-%1 entry-%2 and ID-%3 entry-%4 have same ASIC and channel number.").arg(QString::number(i/2), QString::number(i%2+1),
                                                                                                                QString::number(j/2), QString::number(j%2+1)));
                return false;
            }
        }
    }

    if(!config->coincidenceEnabled)
        config->timeWindow = 100;

    return true;
}

void MainWindow::onConfigChanged()
{
    // quit worker thread, save plot data
    if (!workerThread.isNull()) {
        ui->actionStop->trigger();
        qDebug() << "Writing results to disk";
        workerThread->wait();
        qDebug() << "Result saving done";
        ui->statusBar->showMessage("Stopped.");
    }
}

void MainWindow::onConfigApplied()
{
    // user press Start button

    // if config is not changed, i.e., worker thread is running
    // do nothing
    if (!workerThread.isNull()){
        return;
    }
    // otherwise, updating config is finished
    // check config
    if (!checkConfig())
        return;
    // create plot data if not already
    if (!plotdata)
        plotdata = new PlotData(config);
    else {
        // clear plot data
        plotdata->clearAll();
    }
    // check if image space changed
    if (imageSpacechaged){
        if (plotdata){
            plotdata->rebinImage();
            plotdata->rebinSpectrum();
            plotdata->rebinPairSpectra();
            plotdata->rebinLGSpectra();
            plotdata->rebinHGSpectra();
            if(!plotwindow.isNull())
                plotwindow->rebinImage();
        }
        imageSpacechaged = false;
    }
    // pop up plot window
    if (plotwindow.isNull() || !plotwindow->isVisible()){
        plotwindow = new QPlotWindow(this, config, plotdata);
        handleOpenplot();
    }
    // start worker thread
    if (workerThread.isNull())
    {
        // update image on worker thread
        workerThread = new Worker(this, config, plotdata);
        connect(workerThread, &Worker::finished, workerThread, &QObject::deleteLater);
        connect(workerThread, &Worker::finished, this, &MainWindow::notifyThreadFinished);
        connect(this, &MainWindow::workerStarted, workerThread, &Worker::handleStart);
        connect(ui->actionPause, &QAction::triggered, workerThread, &Worker::handlePause);
        connect(ui->actionStop, &QAction::triggered, workerThread, &Worker::handleStop);
        workerThread->start();
    }

    ui->statusBar->showMessage("Running.");
//    // save current setting
//    config->writeSetup(config->outputDir.absolutePath() + "/settings.ini");
//    qDebug() << "Write settings to " << config->outputDir.absolutePath();
}

// General
void MainWindow::onOpenFileClicked()
{
    // open data file for processing
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Data File"), QDir::currentPath(), tr("Binary Files (*.data *.bin);;Text Files (*.txt *.csv *.data)"));
    if (!fileName.isEmpty() && fileName!=config->filePath){
        onConfigChanged();
        config->filePath = fileName;
        ui->filePathLabel->setText(config->filePath);
    }
}

void MainWindow::oninputFormatChanged(const QString text)
{
    if (text != config->inputFormat)
    {
        onConfigChanged();
        config->inputFormat = text;
    }
}

void MainWindow::onRunIDInput()
{
    // set run id
    QString newid = ui->runIDInput->text();
    if (newid != config->runID){
        onConfigChanged();
        config->runID = newid;
        config->outputDir = QDir(newid);
    }
}

void MainWindow::onMaxEventNumInput()
{
    int eventNum = ui->maxEventNumInput->text().toInt();
    if (eventNum != config->maxN){
        onConfigChanged();
        config->maxN = eventNum;
    }
}

// Plot
void MainWindow::onRadiusInput()
{
    // set radius of projection sphere
    double newradius = ui->radiusInput->text().toDouble();
    if (!doubleEqual(newradius, config->R)){
        onConfigChanged();
        config->R = newradius;
        imageSpacechaged = true;
    }
}

void MainWindow::onPhiBinsInput()
{
    int phibins = ui->phiBinsInput->text().toInt();
    if (phibins != config->phiBins){
        onConfigChanged();
        config->phiBins = phibins;
        imageSpacechaged = true;
    }
}
void MainWindow::onPhiMinInput()
{
    double phimin = ui->phiMinInput->text().toDouble();
    phimin *= M_PI / 180;
    if (!doubleEqual(phimin, config->phiMin)){
        onConfigChanged();
        config->phiMin = phimin;
        imageSpacechaged = true;
    }
}
void MainWindow::onPhiMaxInput()
{
    double phimax = ui->phiMaxInput->text().toDouble();
    phimax *= M_PI / 180;
    if (!doubleEqual(phimax, config->phiMax)){
        onConfigChanged();
        config->phiMax = phimax;
        imageSpacechaged = true;
    }
}
void MainWindow::onThetaBinsInput()
{
    int thetabins = ui->thetaBinsInput->text().toInt();
    if (thetabins != config->thetaBins){
        onConfigChanged();
        config->thetaBins = thetabins;
        imageSpacechaged = true;
    }
}
void MainWindow::onThetaMinInput()
{
    double thetamin = ui->thetaMinInput->text().toDouble();
    thetamin *= M_PI / 180;
    if (!doubleEqual(thetamin, config->thetaMin)){
        onConfigChanged();
        config->thetaMin = thetamin;
        imageSpacechaged = true;
    }
}
void MainWindow::onThetaMaxInput()
{
    double thetamax = ui->thetaMaxInput->text().toDouble();
    thetamax *= M_PI / 180;
    if (!doubleEqual(thetamax, config->thetaMax)){
        onConfigChanged();
        config->thetaMax = thetamax;
        imageSpacechaged = true;
    }
}

void MainWindow::onErgBinsInput()
{
    int ergBins = ui->ergBinsInput->text().toInt();
    if (ergBins != config->ergBins){
        onConfigChanged();
        config->ergBins = ergBins;
        imageSpacechaged = true;
    }
}

void MainWindow::onErgMinInput()
{
    double ergMin = ui->ergMinInput->text().toDouble();
    if (!doubleEqual(ergMin, config->ergMin))
    {
        onConfigChanged();
        config->ergMin = ergMin;
        imageSpacechaged = true;
    }
}

void MainWindow::onErgMaxInput()
{
    double ergMax = ui->ergMaxInput->text().toDouble();
    if (!doubleEqual(ergMax, config->ergMax))
    {
        onConfigChanged();
        config->ergMax = ergMax;
        imageSpacechaged = true;
    }
}

void MainWindow::onErgCutStateChanged()
{
    bool ergCutEnabled = ui->enableErgCutBox->isChecked();
    if (ergCutEnabled != config->energyCut)
    {
        onConfigChanged();
        config->energyCut = ergCutEnabled;
    }
}

void MainWindow::onErgCutLowInput()
{
    double ergLow = ui->ergCutLowInput->text().toDouble();
    if (!doubleEqual(ergLow, config->energyLow))
    {
        onConfigChanged();
        config->energyLow = ergLow;
    }
}

void MainWindow::onErgCutHighInput()
{
    double ergHigh = ui->ergCutHighInput->text().toDouble();
    if (!doubleEqual(ergHigh, config->energyUp))
    {
        onConfigChanged();
        config->energyUp = ergHigh;
    }
}


void MainWindow::onSaveImageStateChanged()
{
    bool saveImage = ui->saveImageBox->isChecked();
    if (saveImage != config->saveImage)
    {
        onConfigChanged();
        config->saveImage = saveImage;
    }
}

void MainWindow::onsaveImageFormatChanged(const QString text)
{
    if (text != config->saveImageFormat)
    {
        onConfigChanged();
        config->saveImageFormat = text;
    }
}

void MainWindow::onSaveSpectrumStateChanged()
{
    bool saveSpectrum = ui->saveSpectrumBox->isChecked();
    if (saveSpectrum != config->saveSpectrum)
    {
        onConfigChanged();
        config->saveSpectrum = saveSpectrum;
    }
}

void MainWindow::onsaveSpectrumFormatChanged(const QString text)
{
    if (text != config->saveSpectrumFormat)
    {
        onConfigChanged();
        config->saveSpectrumFormat = text;
    }
}

void MainWindow::onPairErgBinsInput()
{
    int pairErgBins = ui->pairErgBinInput->text().toInt();
    if (pairErgBins != config->pairErgBins){
        onConfigChanged();
        config->pairErgBins = pairErgBins;
        imageSpacechaged = true;
    }
}
void MainWindow::onPairErgMinInput()
{
    double pairErgMin = ui->pairErgMinInput->text().toDouble();
    if (!doubleEqual(pairErgMin, config->pairErgMin))
    {
        onConfigChanged();
        config->pairErgMin = pairErgMin;
        imageSpacechaged = true;
    }
}
void MainWindow::onPairErgMaxInput()
{
    double pairErgMax = ui->pairErgMaxInput->text().toDouble();
    if (!doubleEqual(pairErgMax, config->pairErgMax))
    {
        onConfigChanged();
        config->pairErgMax = pairErgMax;
        imageSpacechaged = true;
    }
}
void MainWindow::onLGHGBinBox(const QString text)
{
    int bins = text.toInt();
    if(bins != config->LGHGErgBins)
    {
        onConfigChanged();
        config->LGHGErgBins = bins;
        imageSpacechaged = true;
    }
}
void MainWindow::onCoinOnStatChanged()
{
    bool enabled = ui->enableCoincidencebox->isChecked();
    if (enabled != config->coincidenceEnabled)
    {
        onConfigChanged();
        config->coincidenceEnabled = enabled;
    }
}


void MainWindow::onTimeWindowInput()
{
    double tmewin = ui->timeWindowInput->text().toDouble();
    if (!doubleEqual(tmewin, config->timeWindow))
    {
        onConfigChanged();
        config->timeWindow = tmewin;
    }
}

void MainWindow::onStatChanged(int id)
{
    bool newstat = ChEntries[id]->getStat();
    qDebug() << "Index of activated entry = " << id;
    if (newstat != config->channelSettings[2*id].enabled)
    {
        onConfigChanged();
        qDebug() << "Old channel state is " << config->channelSettings[2*id].enabled;
        config->channelSettings[2*id].enabled = newstat;
        config->channelSettings[2*id+1].enabled = newstat;
        qDebug() << "New channelstate is " << config->channelSettings[2*id].enabled;
    }
}

void MainWindow::onPosXChanged(int id)
{
    double newposx = ChEntries[id]->getPosX();
    qDebug() << "Index of activated entry = " << id;
    if (!doubleEqual(newposx, config->channelSettings[2*id].x))
        {
            onConfigChanged();
            qDebug() << "Old x is " << config->channelSettings[2*id].x;
            config->channelSettings[2*id].x = newposx;
            config->channelSettings[2*id+1].x = newposx;
            qDebug() << "New x is " << config->channelSettings[2*id].x;
        }
}

void MainWindow::onPosYChanged(int id)
{
    double newposy = ChEntries[id]->getPosY();
    qDebug() << "Index of activated entry = " << id;
    if (!doubleEqual(newposy, config->channelSettings[2*id].y))
    {
        onConfigChanged();
        qDebug() << "Old y is " << config->channelSettings[2*id].y;
        config->channelSettings[2*id].y = newposy;
        config->channelSettings[2*id+1].y = newposy;
        qDebug() << "New y is " << config->channelSettings[2*id].y;
    }
}

void MainWindow::onPosZChanged(int id, int entry)
{
    double newposz = ChEntries[id]->getPosZ(entry);
    qDebug() << "Index of activated entry = " << id;
    if (!doubleEqual(newposz, config->channelSettings[2*id+entry - 1].z))
    {
        onConfigChanged();
        qDebug() << "Old z is " << config->channelSettings[2*id+entry - 1].z;
        config->channelSettings[2*id+entry - 1].z = newposz;
        qDebug() << "New z is " << config->channelSettings[2*id+entry - 1].z;
    }
}

void MainWindow::onASICIDChanged(int id, int entry)
{
    int newasic = ChEntries[id]->getASICID(entry);
    qDebug() << "Index of activated entry = " << id;
    if(newasic != config->channelSettings[2*id+entry -1].ASICID)
    {
        onConfigChanged();
        qDebug() << "Old ASIC ID is " << config->channelSettings[2*id+entry -1].ASICID;
        config->channelSettings[2*id+entry -1].ASICID = newasic;
        qDebug() << "NewASIC ID is " << config->channelSettings[2*id+entry -1].ASICID;
    }
}

void MainWindow::onchNumChanged(int id, int entry)
{
    int newChNum = ChEntries[id]->getChNum(entry);
    qDebug() << "Index of activated entry = " << id;
    if (newChNum != config->channelSettings[2*id+entry -1].chNum)
    {
        onConfigChanged();
        qDebug() << "Old channel num is " << config->channelSettings[2*id+entry -1].chNum;
        config->channelSettings[2*id+entry -1].chNum = newChNum;
        qDebug() << "New channel num is " << config->channelSettings[2*id+entry -1].chNum;
    }
}

void MainWindow::onSourceChanged(int id, int entry)
{
    QString newsource = ChEntries[id]->getSource(entry);
    qDebug() << "Index of activated entry = " << id;
    if (newsource != config->channelSettings[2*id+entry - 1].source)
    {
        onConfigChanged();
        qDebug() << "Old source is " << config->channelSettings[2*id+entry - 1].source;
        config->channelSettings[2*id+entry - 1].source = newsource;
        qDebug() << "New source num is " << config->channelSettings[2*id+entry - 1].source;
    }
}

void MainWindow::onThreChanged(int id, int entry)
{
    double newthre = ChEntries[id]->getThreshold(entry);
    qDebug() << "Index of activated entry = " << id;
    if (!doubleEqual(newthre, config->channelSettings[2*id+entry - 1].threshold))
    {
        onConfigChanged();
        qDebug() << "Old threshold is " << config->channelSettings[2*id+entry - 1].threshold;
        config->channelSettings[2*id+entry - 1].threshold = newthre;
        qDebug() << "New threshold is " << config->channelSettings[2*id+entry - 1].threshold;
    }
}

void MainWindow::onCaliCoefChanged(int id, int entry)
{
    double newcalicoef = ChEntries[id]->getCaliCoef(entry);
    qDebug() << "Index of activated entry = " << id;
    if (!doubleEqual(newcalicoef, config->channelSettings[2*id+entry - 1].caliCoef))
    {
        onConfigChanged();
        qDebug() << "Old cali coef is " << config->channelSettings[2*id+entry - 1].caliCoef;
        config->channelSettings[2*id+entry - 1].caliCoef = newcalicoef;
        qDebug() << "New cali coef is " << config->channelSettings[2*id+entry - 1].caliCoef;
    }
}

bool MainWindow::doubleEqual(const double& a, const double & b, const double epsilon)
{
    return std::abs(a-b)<=epsilon;
}

void MainWindow::setupMapping()
{
    QGridLayout* glay = ui->mapLayout;
    // first entry, label
    // second entry, all
    connect(ui->AllChannelOnBox, &QCheckBox::stateChanged, this, &MainWindow::onAllStatChanged);
    connect(ui->AllSourceBox, &QComboBox::currentTextChanged, this, &MainWindow::onAllSourceChanged);

    // threshold
    QDoubleValidator* thredv = new QDoubleValidator(0, 10000, 6, ui->AllThreInput);
    thredv->setNotation(QDoubleValidator::StandardNotation);
    ui->AllThreInput->setValidator(thredv);
    connect(ui->AllThreInput, &QLineEdit::editingFinished, this, &MainWindow::onAllThreChanged);

    // cali coef
    QDoubleValidator* calidv = new QDoubleValidator(0, 10000, 6, ui->AllCaliCoefInput);
    calidv->setNotation(QDoubleValidator::StandardNotation);
    ui->AllCaliCoefInput->setValidator(calidv);
    connect(ui->AllCaliCoefInput, &QLineEdit::editingFinished, this, &MainWindow::onAllCaliCoefChanged);
    // 3- 31, channels
    for (int i =0; i < 28; i++) {
        FormEntry* newEntry = new FormEntry(ui->scrollAreaWidgetContents);
        ChEntries.append(newEntry);
        glay->addWidget(newEntry, i+2, 0, 1, 10);
        connect(newEntry, &FormEntry::statChanged, this, &MainWindow::onStatChanged);
        connect(newEntry, &FormEntry::posXChnaged, this, &MainWindow::onPosXChanged);
        connect(newEntry, &FormEntry::posYChanged, this, &MainWindow::onPosYChanged);
        connect(newEntry, &FormEntry::ASICIDChanged, this, &MainWindow::onASICIDChanged);
        connect(newEntry, &FormEntry::chNumChanged, this, &MainWindow::onchNumChanged);
        connect(newEntry, &FormEntry::posZChanged, this, &MainWindow::onPosZChanged);
        connect(newEntry, &FormEntry::sourceChanged, this, &MainWindow::onSourceChanged);
        connect(newEntry, &FormEntry::thresholdCHanged, this, &MainWindow::onThreChanged);
        connect(newEntry, &FormEntry::caliCoefChanged, this, &MainWindow::onCaliCoefChanged);
    }
}

void MainWindow::onAllStatChanged()
{
    bool newstat = ui->AllChannelOnBox->isChecked();
    if(newstat == config->allChannelOn)
        return;
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Change all channels",
                                                               QString("Are you sure you want to change the on/off state of all channels?\n"),
                                                               QMessageBox::Yes | QMessageBox::No,
                                                               QMessageBox::No);
    if (resBtn != QMessageBox::Yes) {
        ui->AllChannelOnBox->setChecked(config->allChannelOn);
        return;
    }
    else
        config->allChannelOn=newstat;
    for (int i=0; i<ChEntries.size() && i < config->channelSettings.size() / 2; i++) {
        if (newstat != ChEntries[i]->getStat())
        {
            ChEntries[i]->setStat(newstat);
            onStatChanged(i);
        }
    }
}

void MainWindow::onAllSourceChanged()
{
    QString newsource = ui->AllSourceBox->currentText();
    if(newsource==config->allSource)
        return;
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Change all channels",
                                                               QString("Are you sure you want to change the signal source of all channels?\n"),
                                                               QMessageBox::Yes | QMessageBox::No,
                                                               QMessageBox::No);
    if (resBtn != QMessageBox::Yes) {
        ui->AllSourceBox->setCurrentText(config->allSource);
        return;
    }
    else
        config->allSource=newsource;

    for (int i=0; i<ChEntries.size() && i < config->channelSettings.size() / 2; i++) {
        for (int j=0; j<2; j++) {
            if (newsource != ChEntries[i]->getSource(j+1))
            {
                ChEntries[i]->setSource(newsource, j+1);
                onSourceChanged(i, j+1);
            }
        }
    }
}

void MainWindow::onAllThreChanged()
{
    double newthre = ui->AllThreInput->text().toDouble();
    if(doubleEqual(newthre, config->allThreshold))
        return;
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Change all channels",
                                                               QString("Are you sure you want to change the energy threshold of all channels?\n"),
                                                               QMessageBox::Yes | QMessageBox::No,
                                                               QMessageBox::No);
    if (resBtn != QMessageBox::Yes) {
        ui->AllThreInput->setText(QString::number(config->allThreshold, 'f', 4));
        return;
    }
    else
        config->allThreshold=newthre;
    for (int i=0; i<ChEntries.size() && i < config->channelSettings.size() / 2; i++) {
        for (int j=0; j<2; j++) {
            if (!doubleEqual(newthre, ChEntries[i]->getThreshold(j+1)))
            {
                ChEntries[i]->setThreshold(newthre, j+1);
                onThreChanged(i, j+1);
            }
        }
    }
}

void MainWindow::onAllCaliCoefChanged()
{
    double newcali = ui->AllCaliCoefInput->text().toDouble();
    if(newcali == config->allCaliCoef)
        return;
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Change all channels",
                                                               QString("Are you sure you want to change the calibration coefficient of all channels?\n"),
                                                               QMessageBox::Yes | QMessageBox::No,
                                                               QMessageBox::No);
    if (resBtn != QMessageBox::Yes) {
        ui->AllCaliCoefInput->setText(QString::number(config->allCaliCoef, 'f', 4));
        return;
    }
    else
        config->allCaliCoef = newcali;
    for (int i=0; i<ChEntries.size() && i < config->channelSettings.size() / 2; i++) {
        for (int j=0; j<2; j++) {
            if (!doubleEqual(newcali, ChEntries[i]->getCaliCoef(j+1)))
            {
                ChEntries[i]->setCaliCoef(newcali, j+1);
                onCaliCoefChanged(i, j+1);
            }
        }
    }
}

