#include "qcustomcanvas.h"
#include <QFileInfo>
#include <QDebug>

QCustomCanvas::QCustomCanvas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlotCanvas)
{
    ui->setupUi(this);
    ui->canvas->setZoomMode(true);
    connect(ui->ClearButton, &QToolButton::clicked, this, &QCustomCanvas::handleClear);
    connect(ui->UnZoomButton, &QToolButton::clicked, this, &QCustomCanvas::handleZoomout);
    connect(ui->LogButton, &QToolButton::clicked, this, &QCustomCanvas::handleLogscaled);
//    #ifdef _WIN32
//    ui->ScreenshotButton->hide();
//    #else
//    connect(ui->ScreenshotButton, &QToolButton::clicked, this, &QCustomCanvas::handleScreenshot);
//    #endif
    connect(ui->ScreenshotButton, &QToolButton::clicked, this, &QCustomCanvas::handleScreenshot);
    connect(ui->SaveButton, &QToolButton::clicked, this, &QCustomCanvas::handleSave2Txt);

    connect(ui->pairBox, &QComboBox::currentTextChanged, this, &QCustomCanvas::handlePairBox);
    connect(ui->asicBox, &QComboBox::currentTextChanged, this, &QCustomCanvas::handleAsicBox);
    connect(ui->channelBox, &QComboBox::currentTextChanged, this, &QCustomCanvas::handleChannelBox);
    connect(ui->sourceBox, &QComboBox::currentTextChanged, this, &QCustomCanvas::handleSourceBox);
}

QCustomCanvas::~QCustomCanvas()
{
    delete ui;
}

int QCustomCanvas::getID()
{
    return id;
}

bool QCustomCanvas::getLogScaled()
{
    return ui->LogButton->isChecked();
}

void QCustomCanvas::setCountsLabel(int counts)
{
    ui->countsLabel->setText(QString("Total counts: %1").arg(QString::number(counts)));
}

int QCustomCanvas::getAsicID()
{
    return ui->asicBox->currentText().toInt();
}

int QCustomCanvas::getChannelID()
{
    return ui->channelBox->currentText().toInt();
}

int QCustomCanvas::getPairID()
{
    return ui->pairBox->currentText().toInt();
}

QString QCustomCanvas::getSource()
{
    return ui->sourceBox->currentText();
}

void QCustomCanvas::handleSave2Txt()
{
    qDebug() << "save canvas to txt";
    emit canvasSaved2Txt(id);
}
void QCustomCanvas::handleClear()
{
    qDebug() << "clear canvas";
    emit canvasCleared(id);
}
void QCustomCanvas::handleZoomout()
{
    qDebug() << "zoom out";
    emit canvasZoomedout(id);
}

void QCustomCanvas::handleScreenshot()
{
    qDebug() << "save canvas to image";
    emit canvasSaved2Image(id);
}

void QCustomCanvas::handleLogscaled()
{
    qDebug() << "Set log scale to " << ui->LogButton->isChecked();
    emit canvasLogscaled(id);
}

void QCustomCanvas::handleAsicBox()
{
    emit asicChanged(id);
}

void QCustomCanvas::handleChannelBox()
{
    emit channelChanged(id);
}

void QCustomCanvas::handlePairBox()
{
    emit pairChanged(id);
}

void QCustomCanvas::handleSourceBox()
{
    emit sourceChanged(id);
}

void QCustomCanvas::hideLGHGBoxes()
{
    ui->sourceLabel->hide();
    ui->sourceBox->hide();
    ui->asicLabel->hide();
    ui->asicBox->hide();
    ui->channelLabel->hide();
    ui->channelBox->hide();
}

void QCustomCanvas::hidePairBox()
{
    ui->pairLabel->hide();
    ui->pairBox->hide();
}

void QCustomCanvas::hideBoxes()
{
    hidePairBox();
    hideLGHGBoxes();
}

void QCustomCanvas::hideLogButton()
{
    ui->LogButton->hide();
}
