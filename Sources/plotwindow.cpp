#include "plotwindow.h"
#include <QDebug>
#include <QDateTime>

QPlotWindow::QPlotWindow(QWidget *parent, const Setup* config_, PlotData* plotdata_)
    : QMainWindow(parent),
      config(config_),
      ui(new Ui::PlotWindow),
      plotdata(plotdata_)
{
    ui->setupUi(this);
    ui->imageCanvas->setID(1);
    ui->imageCanvas->getCanvas()->setAutoScale(false);
    ui->imageCanvas->hideBoxes();
    ui->imageCanvas->hideLogButton();
    ui->spectrumCanvas->setID(2);
    // hide asic and channel boxes
    // for image and spectrum canvas
    ui->spectrumCanvas->hideBoxes();

    ui->pairCanvas->setID(3);
    ui->pairCanvas->hideLGHGBoxes();
    ui->LGHGCanvas->setID(4);
    ui->LGHGCanvas->hidePairBox();

    connect(ui->actionStart, &QAction::triggered, this, &QPlotWindow::handleStart);
    connect(ui->actionPause, &QAction::triggered, this, &QPlotWindow::handlePause);
    connect(ui->actionStop, &QAction::triggered, this, &QPlotWindow::handleStop);
    connect(ui->actionUnzoom, &QAction::triggered, this, &QPlotWindow::handleUnzoom);
    connect(ui->actionClearAll, &QAction::triggered, this, &QPlotWindow::handleClear);

    connect(ui->imageCanvas, &QCustomCanvas::canvasZoomedout, this, &QPlotWindow::handleUnzoom);
    connect(ui->spectrumCanvas, &QCustomCanvas::canvasZoomedout, this, &QPlotWindow::handleUnzoom);
    connect(ui->imageCanvas, &QCustomCanvas::canvasCleared, this, &QPlotWindow::handleClear);
    connect(ui->spectrumCanvas, &QCustomCanvas::canvasCleared, this, &QPlotWindow::handleClear);
    connect(ui->imageCanvas, &QCustomCanvas::canvasSaved2Image, this, &QPlotWindow::handleScreenshot);
    connect(ui->spectrumCanvas, &QCustomCanvas::canvasSaved2Image, this, &QPlotWindow::handleScreenshot);
    connect(ui->imageCanvas, &QCustomCanvas::canvasSaved2Txt, this, &QPlotWindow::handleSave2txt);
    connect(ui->spectrumCanvas, &QCustomCanvas::canvasSaved2Txt, this, &QPlotWindow::handleSave2txt);

    connect(ui->pairCanvas, &QCustomCanvas::canvasZoomedout, this, &QPlotWindow::handleUnzoom);
    connect(ui->LGHGCanvas, &QCustomCanvas::canvasZoomedout, this, &QPlotWindow::handleUnzoom);
    connect(ui->pairCanvas, &QCustomCanvas::canvasCleared, this, &QPlotWindow::handleClear);
    connect(ui->LGHGCanvas, &QCustomCanvas::canvasCleared, this, &QPlotWindow::handleClear);
    connect(ui->pairCanvas, &QCustomCanvas::canvasSaved2Image, this, &QPlotWindow::handleScreenshot);
    connect(ui->LGHGCanvas, &QCustomCanvas::canvasSaved2Image, this, &QPlotWindow::handleScreenshot);
    connect(ui->pairCanvas, &QCustomCanvas::canvasSaved2Txt, this, &QPlotWindow::handleSave2txt);
    connect(ui->LGHGCanvas, &QCustomCanvas::canvasSaved2Txt, this, &QPlotWindow::handleSave2txt);

    connect(ui->LGHGCanvas, &QCustomCanvas::asicChanged, this, &QPlotWindow::handleAsicChanged);
    connect(ui->LGHGCanvas, &QCustomCanvas::channelChanged, this, &QPlotWindow::handleChannelChanged);

    connect(ui->LGHGCanvas, &QCustomCanvas::sourceChanged, this, &QPlotWindow::handleSourceChanged);
    connect(ui->pairCanvas, &QCustomCanvas::pairChanged, this, &QPlotWindow::handlePairChanged);



    // draw image
    setupImage(ui->imageCanvas->getCanvas());

    // draw spectrum
    setupCoinSpectrum(ui->spectrumCanvas->getCanvas());

    // draw single pair spectrum
    setupPairSpectrum(ui->pairCanvas->getCanvas());

    // draw LG/HG spectrum
    setupLGHGSpectrum(ui->LGHGCanvas->getCanvas());

    // update every 1 second
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &QPlotWindow::redraw);
    timer->start(1000);
}


QPlotWindow::~QPlotWindow()
{
    delete ui;
}

void QPlotWindow::setupImage(QCustomPlot* customPlot)
{
    // configure axis rect:
    customPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom); // this will also allow rescaling the color scale by dragging/zooming
    customPlot->axisRect()->setupFullAxesBox(true);
    customPlot->xAxis->setLabel("Azimuth angle (degree)");
    customPlot->yAxis->setLabel("Elevation angle (degree)");

    // set up the QCPColorMap:
    const double conv = 180 / M_PI;
    image = new QCPColorMap(customPlot->xAxis, customPlot->yAxis);
    image->data()->setSize(config->phiBins, config->thetaBins); // we want the color map to have nx * ny data points
    image->data()->setRange(QCPRange(config->phiMin*conv, config->phiMax*conv),
                            QCPRange(config->thetaMin*conv, config->thetaMax*conv)); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions
    // now we assign some data, by accessing the QCPColorMapData instance of the color map:
    plotdata->mMutex.lock();
    const std::vector<std::vector<double>>& data = plotdata->getImageData();
    for (std::size_t i=0; i < data.size(); i++) {
        for (std::size_t j=0; j < data[0].size(); j++) {
            image->data()->setCell(j, i, data[i][j]);
        }
    }
    ui->imageCanvas->setCountsLabel(plotdata->getImageCounts());
    plotdata->mMutex.unlock();


    // add a color scale:
    QCPColorScale *colorScale = new QCPColorScale(customPlot);
    customPlot->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
    colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    image->setColorScale(colorScale); // associate the color map with the color scale
    colorScale->axis()->setLabel("Probability density");

    // set the color gradient of the color map to one of the presets:
    image->setGradient(QCPColorGradient::gpPolar);
    // we could have also created a QCPColorGradient instance and added own colors to
    // the gradient, see the documentation of QCPColorGradient for what's possible.

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    image->rescaleDataRange(true);

    // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
    QCPMarginGroup *marginGroup = new QCPMarginGroup(customPlot);
    customPlot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

    // rescale the key (x) and value (y) axes so the whole color map is visible:
    customPlot->rescaleAxes();
}

void QPlotWindow::updateImage(QCustomPlot* customPlot)
{
    plotdata->mMutex.lock();
    const std::vector<std::vector<double>>& data = plotdata->getImageData();
    if(data.size() != image->data()->valueSize())
    {
        qDebug() << "Old theta bin" << image->data()->valueSize() << "New theta bin" << data.size();
    }
    if(data[0].size() != image->data()->keySize())
    {
        qDebug() << "Old phi bin" << image->data()->keySize() << "New phi bin" << data[0].size();
    }
    if(data[0].size() != image->data()->keySize() || data.size() != image->data()->valueSize())
    {
        const double conv = 180 / M_PI;
        image->data()->setSize(config->phiBins, config->thetaBins); // we want the color map to have nx * ny data points
        image->data()->setRange(QCPRange(config->phiMin*conv, config->phiMax*conv),
                                QCPRange(config->thetaMin*conv, config->thetaMax*conv)); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions
    }
    for (std::size_t i=0; i < data.size(); i++) {
        for (std::size_t j=0; j < data[0].size(); j++) {
            image->data()->setCell(j, i, data[i][j]);
        }
    }

    ui->imageCanvas->setCountsLabel(plotdata->getImageCounts());
    plotdata->mMutex.unlock();

    image->rescaleDataRange(true);
    //        image->rescaleAxes();
    customPlot->replot();
}

void QPlotWindow::rebinImage()
{
    image->data()->clear();
    const double conv = 180 / M_PI;
    image->data()->setSize(config->phiBins, config->thetaBins); // we want the color map to have nx * ny data points
    image->data()->setRange(QCPRange(config->phiMin*conv, config->phiMax*conv),
                            QCPRange(config->thetaMin*conv, config->thetaMax*conv)); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions

    image->rescaleAxes();
}

void QPlotWindow::setupCoinSpectrum(QCustomPlot* customPlot)
{
    // configure axis rect:
    customPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom); // this will also allow rescaling the color scale by dragging/zooming
    customPlot->axisRect()->setupFullAxesBox(true);
    customPlot->xAxis->setLabel("Total energy deposited (keV)");
    customPlot->yAxis->setLabel("Counts");

    // setup step line plot
    customPlot->addGraph();
    plotdata->mMutex.lock();
    customPlot->graph()->setData(plotdata->getCoinBinCenters(), plotdata->getCoinBinCounts(), true);
    ui->spectrumCanvas->setCountsLabel(plotdata->getCoinTotalCounts());
    plotdata->mMutex.unlock();
    customPlot->graph()->setLineStyle(QCPGraph::lsStepCenter);
    customPlot->graph()->setBrush(QBrush(QColor(0, 0, 255, 20))); // fill with translucent blue
//    customPlot->rescaleAxes();
    customPlot->yAxis->setRangeLower(0);
//    customPlot->yAxis->setRange(0, 500);
}
void QPlotWindow::setupLGHGSpectrum(QCustomPlot* customPlot)
{
    // configure axis rect:
    customPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom); // this will also allow rescaling the color scale by dragging/zooming
    customPlot->axisRect()->setupFullAxesBox(true);
    customPlot->yAxis->setLabel("Counts");

    // setup step line plot
    customPlot->addGraph();
    if (source == "LG")
    {
        customPlot->xAxis->setLabel("Charge LG");
        plotdata->mMutex.lock();
        customPlot->graph()->setData(plotdata->getLGBinCenters(LGAsicID, LGChannelID),
                                      plotdata->getLGBinCounts(LGAsicID, LGChannelID), true);
        ui->LGHGCanvas->setCountsLabel(plotdata->getLGTotalCounts(LGAsicID, LGChannelID));
        plotdata->mMutex.unlock();
    }
    else if (source== "HG")
    {
        customPlot->xAxis->setLabel("Charge HG");
        plotdata->mMutex.lock();
        customPlot->graph()->setData(plotdata->getHGBinCenters(HGAsicID, HGChannelID),
                                      plotdata->getHGBinCounts(HGAsicID, HGChannelID), true);
        ui->LGHGCanvas->setCountsLabel(plotdata->getHGTotalCounts(HGAsicID, HGChannelID));
        plotdata->mMutex.unlock();
    }
    customPlot->graph()->setLineStyle(QCPGraph::lsStepCenter);
    customPlot->graph()->setBrush(QBrush(QColor(0, 0, 255, 20))); // fill with translucent blue
//    customPlot->rescaleAxes();
    customPlot->yAxis->setRangeLower(0);
//    customPlot->yAxis->setRange(0, 500);
}
void QPlotWindow::setupPairSpectrum(QCustomPlot* customPlot)
{
    // configure axis rect:
    customPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom); // this will also allow rescaling the color scale by dragging/zooming
    customPlot->axisRect()->setupFullAxesBox(true);
    customPlot->xAxis->setLabel("Deposited energy(keV)");
    customPlot->yAxis->setLabel("Counts");

    // setup step line plot
    customPlot->addGraph();
    plotdata->mMutex.lock();
    customPlot->graph()->setData(plotdata->getPairBinCenters(pairID), plotdata->getPairBinCounts(pairID), true);
    ui->pairCanvas->setCountsLabel(plotdata->getPairTotalCounts(pairID));
    plotdata->mMutex.unlock();
    customPlot->graph()->setLineStyle(QCPGraph::lsStepCenter);
    customPlot->graph()->setBrush(QBrush(QColor(0, 0, 255, 20))); // fill with translucent blue
//    customPlot->rescaleAxes();
    customPlot->yAxis->setRangeLower(0);
//    customPlot->yAxis->setRange(0, 500);
}

void QPlotWindow::updateCoinSpectrum(QCustomPlot* customPlot)
{
    customPlot->graph()->data().clear();
    plotdata->mMutex.lock();
    int counts = plotdata->getCoinTotalCounts();
    customPlot->graph()->setData(plotdata->getCoinBinCenters(),
                                 plotdata->getCoinBinCounts(), true);
    plotdata->mMutex.unlock();
    ui->spectrumCanvas->setCountsLabel(counts);
    if (counts > 0 && ui->spectrumCanvas->getCanvas()->isAutoScaled())
    {
        handleUnzoom(ui->spectrumCanvas->getID());
    //        customPlot->rescaleAxes();
    }
    if(counts > 0 && ui->spectrumCanvas->getLogScaled())
        customPlot->yAxis->setScaleType(QCPAxis::stLogarithmic);
    else
        customPlot->yAxis->setScaleType(QCPAxis::stLinear);
    customPlot->replot();
}

void QPlotWindow::updatePairSpectrum(QCustomPlot* customPlot)
{
    customPlot->graph()->data().clear();
    plotdata->mMutex.lock();
    int counts = plotdata->getPairTotalCounts(pairID);
    customPlot->graph()->setData(plotdata->getPairBinCenters(pairID),
                                 plotdata->getPairBinCounts(pairID), true);
    plotdata->mMutex.unlock();
    ui->pairCanvas->setCountsLabel(counts);
    if (counts > 0 && ui->pairCanvas->getCanvas()->isAutoScaled())
    {
        handleUnzoom(ui->pairCanvas->getID());
    }
    if(counts > 0 && ui->pairCanvas->getLogScaled())
        customPlot->yAxis->setScaleType(QCPAxis::stLogarithmic);
    else
        customPlot->yAxis->setScaleType(QCPAxis::stLinear);
    customPlot->replot();
}

void QPlotWindow::updateLGHGSpectrum(QCustomPlot* customPlot)
{
    customPlot->graph()->data().clear();
    int counts(0);
    QVector<double> binCounts;
    QVector<double> binCenters;
    if (source == "LG")
    {
        customPlot->xAxis->setLabel("Charge LG");
        plotdata->mMutex.lock();
        counts = plotdata->getLGTotalCounts(LGAsicID, LGChannelID);
        binCounts = plotdata->getLGBinCounts(LGAsicID, LGChannelID);
        plotdata->mMutex.unlock();
    }
    else if (source== "HG")
    {
        customPlot->xAxis->setLabel("Charge HG");
        plotdata->mMutex.lock();
        counts = plotdata->getHGTotalCounts(HGAsicID, HGChannelID);
        binCounts = plotdata->getHGBinCounts(HGAsicID, HGChannelID);
        plotdata->mMutex.unlock();
    }
    for (int i=0; i<binCounts.size(); i++) {
        binCenters.push_back(i+0.5);
    }
    customPlot->graph()->setData(binCenters,
                          binCounts, true);
    ui->LGHGCanvas->setCountsLabel(counts);
    if (counts > 0 && ui->LGHGCanvas->getCanvas()->isAutoScaled())
    {
        handleUnzoom(ui->LGHGCanvas->getID());
    }
    if(counts > 0 && ui->LGHGCanvas->getLogScaled())
        customPlot->yAxis->setScaleType(QCPAxis::stLogarithmic);
    else
        customPlot->yAxis->setScaleType(QCPAxis::stLinear);
    customPlot->replot();
}
void QPlotWindow::handleStart(){
    emit runStarted();
    ui->statusbar->showMessage("Running.");
}
void QPlotWindow::handlePause()
{
    emit runPaused();
    ui->statusbar->showMessage("Paused.");
}
void QPlotWindow::handleStop()
{
    emit runStopped();
    ui->statusbar->showMessage("Stopped.");
}

void QPlotWindow::handleUnzoom(int id)
{
    CustomPlotZoom* canvas=nullptr;
    bool logscaled;
    if (id==0)
    {
        handleUnzoom(ui->imageCanvas->getID());
        handleUnzoom(ui->spectrumCanvas->getID());
        handleUnzoom(ui->pairCanvas->getID());
        handleUnzoom(ui->LGHGCanvas->getID());
        return;
    }
    else if (id == ui->imageCanvas->getID())
    {
        image->rescaleAxes();
        return;
    }
    else if (id ==ui->spectrumCanvas->getID())
    {
        canvas = ui->spectrumCanvas->getCanvas();
        logscaled = ui->spectrumCanvas->getLogScaled();
    }
    else if (id ==ui->pairCanvas->getID())
    {
        canvas = ui->pairCanvas->getCanvas();
        logscaled = ui->pairCanvas->getLogScaled();
    }
    else if (id ==ui->LGHGCanvas->getID())
    {
        canvas = ui->LGHGCanvas->getCanvas();
        logscaled = ui->LGHGCanvas->getLogScaled();
    }
    else
        return;
    canvas->rescaleAxes();
    canvas->yAxis->scaleRange(1.1);
    if(!logscaled)
    {
        canvas->yAxis->setRangeLower(0);
    }
    else
    {
        canvas->yAxis->setRangeLower(0.1);
    }
    canvas->setAutoScale(true);
}

void QPlotWindow::reopen()
{
    ui->imageCanvas->getCanvas()->replot();

    ui->spectrumCanvas->getCanvas()->replot();

    ui->pairCanvas->getCanvas()->replot();

    ui->LGHGCanvas->getCanvas()->replot();
}

void QPlotWindow::redraw()
{
    updateImage(ui->imageCanvas->getCanvas());

    updateCoinSpectrum(ui->spectrumCanvas->getCanvas());

    updatePairSpectrum(ui->pairCanvas->getCanvas());

    updateLGHGSpectrum(ui->LGHGCanvas->getCanvas());

}

void QPlotWindow::handleClear(int id){
    if (id==0){
        qDebug() << "Clear all";
        plotdata->clearAll();
    }
    else if (id ==ui->imageCanvas->getID()) {
        qDebug() << "Clear image";
        plotdata->clearImage();
    }
    else if (id ==ui->spectrumCanvas->getID()) {
        qDebug() << "Clear spectrum";
        plotdata->clearSpectrum();
    }
    else if (id == ui->pairCanvas->getID())
    {
        qDebug() << "Clear single pair spectrum";
        plotdata->clearPairSpectra();
    }
    else if (id == ui->LGHGCanvas->getID())
    {
        qDebug() << "Clear LG/HG spectrum";
        plotdata->clearLGHG();
    }
}

void QPlotWindow::handleScreenshot(int id){
    QCustomPlot* canvas;
    QString key;
    QDateTime dateTime = QDateTime::currentDateTime();
    QString currentDateTime = dateTime.toString("yyyy-MM-dd-HH-mm-ss");
    QString fileName;
    if (id==0){
        qDebug() << "Save all";
        handleScreenshot(ui->imageCanvas->getID());
        handleScreenshot(ui->spectrumCanvas->getID());
        handleScreenshot(ui->pairCanvas->getID());
        handleScreenshot(ui->LGHGCanvas->getID());
        return;
    }
    else if (id ==ui->imageCanvas->getID()) {
        key = "Image";
        fileName = config->outputDir.absolutePath() + QString("/Screenshot-%1-%2.png").arg(key, currentDateTime);

        canvas = ui->imageCanvas->getCanvas();
    }
    else if (id ==ui->spectrumCanvas->getID()) {
        key = "Coincidence-Spectrum";
        canvas = ui->spectrumCanvas->getCanvas();
        fileName = config->outputDir.absolutePath() + QString("/Screenshot-%1-%2.png").arg(key, currentDateTime);
    }
    else if (id ==ui->pairCanvas->getID()) {
        key = "Pair-Spectrum";
        canvas = ui->pairCanvas->getCanvas();
        fileName = config->outputDir.absolutePath() +
                QString("/Screenshot-%1-%2-Pair-%3.png").arg(key, currentDateTime,
                                                                        QString::number(pairID));
    }
    else if (id ==ui->LGHGCanvas->getID()) {
        key = source + "-Spectrum";
        canvas = ui->LGHGCanvas->getCanvas();
        if(source == "LG")
            fileName = config->outputDir.absolutePath() +
                    QString("/Screenshot-%1-%2-ASIC-%3-Channel-%4.png").arg(key, currentDateTime,
                                                                            QString::number(LGAsicID), QString::number(LGChannelID));
        else if (source == "HG")
            fileName = config->outputDir.absolutePath() +
                    QString("/Screenshot-%1-%2-ASIC-%3-Channel-%4.png").arg(key, currentDateTime,
                                                                            QString::number(HGAsicID), QString::number(HGChannelID));
    }
    else
        return;

    qDebug() << QString("Save %1").arg(key);
    canvas->savePng(fileName, 500, 400);
    ui->statusbar->showMessage(QString("%1 saved to: %2").arg(key, fileName));
}

void QPlotWindow::handleLogscale(int id)
{
    if(id==0)
    {
        qDebug() << "Log all";
        handleLogscale(ui->spectrumCanvas->getID());
        handleLogscale(ui->pairCanvas->getID());
        handleLogscale(ui->LGHGCanvas->getID());
        return;
    }
    else if (id==ui->spectrumCanvas->getID())
    {
        updateCoinSpectrum(ui->spectrumCanvas->getCanvas());
    }
    else if (id==ui->pairCanvas->getID())
    {
        updatePairSpectrum(ui->pairCanvas->getCanvas());
    }
    else if (id==ui->LGHGCanvas->getID())
    {
        updateLGHGSpectrum(ui->LGHGCanvas->getCanvas());
    }
}

void QPlotWindow::handleSave2txt(int id){

    QDateTime dateTime = QDateTime::currentDateTime();
    QString currentDateTime = dateTime.toString("yyyy-MM-dd-HH-mm-ss");
    if (id==0){
        qDebug() << "Save all";
        handleSave2txt(ui->imageCanvas->getID());
        handleSave2txt(ui->spectrumCanvas->getID());
        handleSave2txt(ui->pairCanvas->getID());
        handleSave2txt(ui->LGHGCanvas->getID());
        return;
    }
    else if (id == ui->imageCanvas->getID()) {
        qDebug() << "Save image to txt";
        QString fileName = config->outputDir.absolutePath() + QString("/Image-%1.txt").arg(currentDateTime);
        plotdata->saveImage2txt(fileName.toStdString());
        ui->statusbar->showMessage(QString("Image saved to: %1").arg(fileName));
    }
    else if (id == ui->spectrumCanvas->getID()) {
        qDebug() << "Save coincidence spectrum to txt";
        QString fileName = config->outputDir.absolutePath() + QString("/Coincidence-Spectrum-%1.txt").arg(currentDateTime);
        plotdata->saveSpectrum2txt(fileName.toStdString());
        ui->statusbar->showMessage(QString("Spectrum saved to: %1").arg(fileName));
    }
    else if (id == ui->pairCanvas->getID()) {
        qDebug() << "Save pai spectrum to txt";
        QString fileName = config->outputDir.absolutePath() +
                QString("/Pair-Spectrum-%1-ID-%2.txt").arg(currentDateTime,
                                                           QString::number(pairID));
        plotdata->savePairSpectrum2txt(fileName.toStdString(), pairID);
        ui->statusbar->showMessage(QString("LG Spectrum saved to: %1").arg(fileName));
    }
    else if (id == ui->LGHGCanvas->getID()) {
        qDebug() << "Save LG/HG spectrum to txt";
        QString fileName;
        if (source=="LG")
        {
            fileName = config->outputDir.absolutePath() +
                            QString("/LG-Spectrum-%1-ASIC-%2-Channel-%3.txt").arg(currentDateTime,
                                                                                  QString::number(LGAsicID), QString::number(LGChannelID));
            plotdata->saveLGSpectrum2txt(fileName.toStdString(), LGAsicID, LGChannelID);
        }
        else if (source == "HG")
        {
            fileName = config->outputDir.absolutePath() +
                            QString("/HG-Spectrum-%1-ASIC-%2-Channel-%3.txt").arg(currentDateTime,
                                                                                  QString::number(HGAsicID), QString::number(HGChannelID));
            plotdata->saveHGSpectrum2txt(fileName.toStdString(), HGAsicID, HGChannelID);
        }
        else
            return;
        ui->statusbar->showMessage(QString("LG/HG Spectrum saved to: %1").arg(fileName));
    }
}

void QPlotWindow::handlePairChanged(int id)
{
    if (id == ui->pairCanvas->getID())
    {
        int currentID = ui->pairCanvas->getPairID();
        if (currentID == pairID)
            return;
        pairID = currentID;
        switchPair(id);
    }
}

void QPlotWindow::handleAsicChanged(int id)
{
 if (id == ui->LGHGCanvas->getID())
 {
     int currentID = (source =="LG") ? LGAsicID : HGAsicID;
     if (ui->LGHGCanvas->getAsicID() == currentID)
         return;
     if (source == "LG")
         LGAsicID = ui->LGHGCanvas->getAsicID();
     else
         HGAsicID = ui->LGHGCanvas->getAsicID();
     switchLGHG(id);
 }
}

void QPlotWindow::handleChannelChanged(int id)
{
    if (id == ui->LGHGCanvas->getID())
    {
        int currentID = (source == "LG") ? LGChannelID : HGChannelID;
        if (ui->LGHGCanvas->getChannelID() == currentID)
            return;
        if (source == "LG")
            LGChannelID = ui->LGHGCanvas->getChannelID();
        else
            HGChannelID = ui->LGHGCanvas->getChannelID();
        switchLGHG(id);
    }
}

void QPlotWindow::handleSourceChanged(int id)
{
    if (id == ui->LGHGCanvas->getID())
    {
        if(ui->LGHGCanvas->getSource() == source)
            return;
        source = ui->LGHGCanvas->getSource();
        if (source == "LG")
        {
            LGAsicID = HGAsicID;
            LGChannelID = HGChannelID;
        }
        else
        {
            HGAsicID = LGAsicID;
            HGChannelID = LGChannelID;
        }
        switchLGHG(id);
//        ui->LGHGCanvas
    }
}

void QPlotWindow::switchLGHG(int id)
{
    if (id != ui->LGHGCanvas->getID())
       return;

    // redraw
    if (source == "LG")
    {
        qDebug() << QString("Draw LG ASIC-%1, Channel-%2").arg(QString::number(LGAsicID), QString::number(LGChannelID));
    }
    else if (source == "HG")
    {

        qDebug() << QString("Draw HG ASIC-%1, Channel-%2").arg(QString::number(HGAsicID), QString::number(HGChannelID));
    }
//    ui->LGHGCanvas->getCanvas()->setAutoScale(true);
    updateLGHGSpectrum(ui->LGHGCanvas->getCanvas());
}

void QPlotWindow::switchPair(int id)
{
    if (id != ui->pairCanvas->getID())
        return;
    // draw new spectrum
    qDebug() << QString("Draw Pair %1").arg(QString::number(pairID));
//    ui->pairCanvas->getCanvas()->setAutoScale(true);
    updatePairSpectrum(ui->pairCanvas->getCanvas());
}

