#include "setup.h"
#include "QDebug"
#include "QDir"
Setup::Setup(const QString initFile)
    : settingPath(initFile)
{
    readSetup(settingPath);
}
void Setup::readSetup(const QString path)
{
    settingPath = path;
    QSettings settings(path, QSettings::IniFormat);
    filePath = settings.value("File").toString();
    inputFormat = settings.value("InputFormat").toString();
    runID = settings.value("RunID").toString();
    outputDir = QDir(runID);
    maxN = settings.value("MaxEventNum").toInt();


    settings.beginGroup("DataSaving");
    saveImage = settings.value("SaveImage").toBool();
    saveSpectrum = settings.value("SaveSpectrum").toBool();
    saveImageFormat = settings.value("ImageFormat").toString();
    saveSpectrumFormat = settings.value("SpectrumFormat").toString();
    settings.endGroup();

    settings.beginGroup("ImageSpace");
    R = settings.value("Radius").toDouble();
//    R = std::sqrt(3.6*3.6+10*10+0);
//    R= 50;
    phiBins = settings.value("NPhiBins").toInt();
    phiMax = settings.value("PhiMax").toDouble();
    phiMax *= M_PI / 180;
    phiMin = settings.value("PhiMin").toDouble();
    phiMin *= M_PI / 180;
    thetaBins = settings.value("NThetaBins").toInt();
    thetaMax = settings.value("ThetaMax").toDouble();
    thetaMax *= M_PI / 180;
    thetaMin = settings.value("ThetaMin").toDouble();
    thetaMin *= M_PI / 180;
    settings.endGroup();

    settings.beginGroup("EnergySpectrum");
    ergBins = settings.value("NEnergyBins").toInt();
    ergMin  = settings.value("EnergyMin").toDouble();
    ergMax = settings.value("EnergyMax").toDouble();
    settings.endGroup();

    if (channelSettings.size()>0)
        channelSettings.clear();
    settings.beginGroup("Channels");
    QStringList keys = settings.childKeys();
//    foreach (QString key, keys) {
//        channelSettings[QVariant(key).toInt()] = QVariant(settings.value(key)).value<Channel>();
//    }
    foreach (QString key, keys) {
        channelSettings.append(QVariant(settings.value(key)).value<Channel>());
    }
    std::sort(channelSettings.begin(), channelSettings.end(),
             [](const Channel & a, const Channel & b) -> bool
             {
                 return a.id < b.id;
             });
    for (int i=0;i<channelSettings.size(); i++)
        qDebug() << i << channelSettings[i] << '\n';
    settings.endGroup();

    settings.beginGroup("Rejections");
    energyCut = settings.value("EnergyCut").toBool();
    energyLow = settings.value("EnergyLowerThreshold").toDouble();
    energyUp = settings.value("EnergyUpperThreshold").toDouble();
    settings.endGroup();

    settings.beginGroup("PairSpectrum");
    pairErgBins = settings.value("NEnergyBins").toInt();
    pairErgMin = settings.value("EnergyMin").toDouble();
    pairErgMax = settings.value("EnergyMax").toDouble();
    settings.endGroup();

    settings.beginGroup("LGHGSpectrum");
    LGHGErgBins = settings.value("NChargeBins").toInt();
    settings.endGroup();

    settings.beginGroup("Coincidence");
    coincidenceEnabled = settings.value("Enabled").toBool();
    timeWindow = settings.value("TimeWindow").toDouble();
    settings.endGroup();

}
void Setup::writeSetup(const QString path) const
{
    QSettings settings(path, QSettings::IniFormat);
    settings.setValue("File", filePath);
    settings.setValue("InputFormat", inputFormat);
    settings.setValue("RunID", runID);
    settings.setValue("MaxEventNum", maxN);

    settings.beginGroup("ImageSpace");
    settings.setValue("Radius", R);
    settings.setValue("NPhiBins", phiBins);
    settings.setValue("PhiMin", std::round( phiMin * 180 / M_PI));
    settings.setValue("PhiMax", std::round( phiMax * 180 / M_PI));
    settings.setValue("ThetaMin", std::round( thetaMin * 180 / M_PI));
    settings.setValue("ThetaMax", std::round( thetaMax * 180 / M_PI));
    settings.setValue("NThetaBins", thetaBins);
    settings.endGroup();

    settings.beginGroup("EnergySpectrum");
    settings.setValue("NEnergyBins", ergBins);
    settings.setValue("EnergyMin", ergMin);
    settings.setValue("EnergyMax", ergMax);
    settings.endGroup();

    settings.beginGroup("Channels");
//    for (auto i= 0;i < channelSettings.size();i++){
//        settings.setValue(QVariant(i).toString(), QVariant::fromValue(channelSettings[i]));
//    }
    for (int i=0;i<channelSettings.size();i++){
        settings.setValue(QString::number(i), QVariant::fromValue(channelSettings[i]));
    }
    settings.endGroup();

    settings.beginGroup("Rejections");
    settings.setValue("EnergyCut", energyCut);
    settings.setValue("EnergyLowerThreshold", energyLow);
    settings.setValue("EnergyUpperThreshold", energyUp);
    settings.endGroup();

    settings.beginGroup("PairSpectrum");
    settings.setValue("NEnergyBins", pairErgBins);
    settings.setValue("EnergyMin", pairErgMin);
    settings.setValue("EnergyMax", pairErgMax);
    settings.endGroup();

    settings.beginGroup("LGHGSpectrum");
    settings.setValue("NChargeBins", LGHGErgBins);
    settings.endGroup();

    settings.beginGroup("Coincidence");
    settings.setValue("Enabled", coincidenceEnabled);
    settings.setValue("TimeWindow", timeWindow);
    settings.endGroup();

    settings.beginGroup("DataSaving");
    settings.setValue("SaveImage", saveImage);
    settings.setValue("SaveSpectrum", saveSpectrum);
    settings.setValue("ImageFormat", saveImageFormat);
    settings.setValue("SpectrumFormat", saveSpectrumFormat);
    settings.endGroup();
}

Setup::~Setup()
{
    qDebug() << "save settings to disk";
//    qDebug() << QDir::current();
    writeSetup();
    qDebug() << "settings saved";
}



