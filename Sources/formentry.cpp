#include "formentry.h"
#include <QCheckBox>
#include <QIntValidator>
#include <QDoubleValidator>
#include <QDebug>

FormEntry::FormEntry(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::FormEntry)
{
    ui->setupUi(this);

    // on/off status
    connect(ui->pairOnBox, &QCheckBox::stateChanged, this, &FormEntry::onStatChanged);

    // pos x, cm
    QDoubleValidator* posxdv = new QDoubleValidator(-1000000, 1000000, 6, ui->posXInput);
    posxdv->setNotation(QDoubleValidator::StandardNotation);
    ui->posXInput->setValidator(posxdv);
    connect(ui->posXInput, &QLineEdit::editingFinished, this, &FormEntry::onPosXChanged);

    // pos y, cm
    QDoubleValidator* posydv = new QDoubleValidator(-1000000, 1000000, 6, ui->posYInput);
    posydv->setNotation(QDoubleValidator::StandardNotation);
    ui->posYInput->setValidator(posydv);
    connect(ui->posYInput, &QLineEdit::editingFinished, this, &FormEntry::onPosYChanged);

    // ASIC ID, {0, 1, 2, 3}
//    QIntValidator* asicv = new QIntValidator(0, 3, ui->ASICIDInput_1);
//    ui->ASICIDInput_1->setValidator(asicv);
//    ui->ASICIDInput_2->setValidator(asicv);
    connect(ui->ASICIDInput_1, &QComboBox::currentTextChanged, this, &FormEntry::onASICIDChanged);
    connect(ui->ASICIDInput_2, &QComboBox::currentTextChanged, this, &FormEntry::onASICIDChanged);

    // channel number, {0, 1, ..., 31}
//    QIntValidator* chNumv = new QIntValidator(0, 31, ui->channelNumInput_1);
//    ui->channelNumInput_1->setValidator(chNumv);
//    ui->channelNumInput_2->setValidator(chNumv);
    connect(ui->channelNumInput_1, &QComboBox::currentTextChanged, this, &FormEntry::onchNumChanged);
    connect(ui->channelNumInput_2, &QComboBox::currentTextChanged, this, &FormEntry::onchNumChanged);

    // pos z, cm
    QDoubleValidator* poszdv = new QDoubleValidator(-1000000, 1000000, 6, ui->posZInput_1);
    poszdv->setNotation(QDoubleValidator::StandardNotation);
    ui->posZInput_1->setValidator(poszdv);
    connect(ui->posZInput_1, &QLineEdit::editingFinished, this, &FormEntry::onPosZChanged);
    ui->posZInput_2->setValidator(poszdv);
    connect(ui->posZInput_2, &QLineEdit::editingFinished, this, &FormEntry::onPosZChanged);

    // input source. LG or HG
    connect(ui->source_1, &QComboBox::currentTextChanged, this, &FormEntry::onSourceChanged);
    connect(ui->source_2, &QComboBox::currentTextChanged, this, &FormEntry::onSourceChanged);

    // threshold
    QDoubleValidator* thredv = new QDoubleValidator(0, 1000000, 6, ui->thresholdInput_1);
    thredv->setNotation(QDoubleValidator::StandardNotation);
    ui->thresholdInput_1->setValidator(thredv);
    ui->thresholdInput_2->setValidator(thredv);
    connect(ui->thresholdInput_1, &QLineEdit::editingFinished, this, &FormEntry::onThreChanged);
    connect(ui->thresholdInput_2, &QLineEdit::editingFinished, this, &FormEntry::onThreChanged);

    // cali coef
    QDoubleValidator* calidv = new QDoubleValidator(0, 1000000, 6, ui->caliCoefInput_1);
    calidv->setNotation(QDoubleValidator::StandardNotation);
    ui->caliCoefInput_1->setValidator(calidv);
    ui->caliCoefInput_2->setValidator(calidv);
    connect(ui->caliCoefInput_1, &QLineEdit::editingFinished, this, &FormEntry::onCaliCoefChanged);
    connect(ui->caliCoefInput_2, &QLineEdit::editingFinished, this, &FormEntry::onCaliCoefChanged);
}

// ID
int FormEntry::getId()
{
    return UID;
}

void FormEntry::setId(const int id)
{
    UID = id;
    ui->label->setText(QString::number(id));
}

// stat
bool FormEntry::getStat()
{
    return ui->pairOnBox->isChecked();
}

void FormEntry::setStat(const bool stat)
{
    ui->pairOnBox->setChecked(stat);
}

void FormEntry::onStatChanged()
{
    emit statChanged(UID);
}

// pos X
double FormEntry::getPosX()
{
    return ui->posXInput->text().toDouble();
}

void FormEntry::setPosX(const double x)
{
    ui->posXInput->setText(QString::number(x, 'f', 4));
    ui->posXInput->setReadOnly(true);
}

void FormEntry::onPosXChanged()
{
    emit posXChnaged(UID);
}

// pos Y
double FormEntry::getPosY()
{
    return ui->posYInput->text().toDouble();
}

void FormEntry::setPosY(const double y)
{
    ui->posYInput->setText(QString::number(y, 'f', 4));
    ui->posYInput->setReadOnly(true);
}

void FormEntry::onPosYChanged()
{
    emit posYChanged(UID);
}

// ASIC id
int FormEntry::getASICID(int entry)
{
    if(entry == 1)
        return ui->ASICIDInput_1->currentText().toInt();
    else if (entry == 2)
        return ui->ASICIDInput_2->currentText().toInt();

    return -1;
}

void FormEntry::setASICID(const int asicid, const int entry)
{
    QComboBox* it;
    if (entry == 1)
        it = ui->ASICIDInput_1;
    else if (entry == 2)
        it = ui->ASICIDInput_2;
    else
        return;
    it->setCurrentText(QString::number(asicid));
}

void FormEntry::onASICIDChanged()
{
    // find sender
    QComboBox* sender_ = qobject_cast<QComboBox*>(sender());
    if (!sender_)
        return;
    if(sender_ == ui->ASICIDInput_1)
    {
        emit ASICIDChanged(UID, 1);
    }
    else if (sender_ == ui->ASICIDInput_2) {
        emit ASICIDChanged(UID, 2);
    }
    else
    {
        qDebug() << "Sender not found";
    }

}

// channel number
int FormEntry::getChNum(int entry)
{
    if(entry == 1)
        return ui->channelNumInput_1->currentText().toInt();
    else if (entry == 2)
        return ui->channelNumInput_2->currentText().toInt();

    return -1;
}

void FormEntry::setChNum(const int chnum, const int entry)
{
    QComboBox* it;
    if (entry == 1)
        it = ui->channelNumInput_1;
    else if (entry == 2)
        it = ui->channelNumInput_2;
    else
        return;
    it->setCurrentText(QString::number(chnum));
}

void FormEntry::onchNumChanged()
{
    // find sender
    QComboBox* sender_ = qobject_cast<QComboBox*>(sender());
    if (!sender_)
        return;
    if(sender_ == ui->channelNumInput_1)
    {
        emit chNumChanged(UID, 1);
    }
    else if (sender_ == ui->channelNumInput_2) {
        emit chNumChanged(UID, 2);
    }
    else
    {
        qDebug() << "Sender not found";
    }

}

// pos Z
double FormEntry::getPosZ(int entry)
{
    if (entry == 1)
        return ui->posZInput_1->text().toDouble();
    else if (entry == 2)
        return ui->posZInput_2->text().toDouble();
    else
        return -10000;

}

void FormEntry::setPosZ(const double z, const int entry)
{
    QLineEdit* it;
    if (entry == 1)
        it = ui->posZInput_1;
    else if (entry == 2)
        it = ui->posZInput_2;
    else
        return;
    it->setText(QString::number(z, 'f', 4));
    it->setReadOnly(true);
}

void FormEntry::onPosZChanged()
{
    // find sender
    QLineEdit* sender_ = qobject_cast<QLineEdit*>(sender());
    if (!sender_)
        return;
    if(sender_ == ui->posZInput_1)
    {
        emit posZChanged(UID, 1);
    }
    else if (sender_ == ui->posZInput_2) {
        emit posZChanged(UID, 2);
    }
    else
    {
        qDebug() << "Sender not found";
    }
}

// input source
QString FormEntry::getSource(int entry)
{
    if (entry == 1)
        return ui->source_1->currentText();
    else if (entry == 2)
        return ui->source_2->currentText();
    else
        return "";
}

void FormEntry::setSource(const QString src, const int entry)
{
    QComboBox* it;
    if (entry == 1)
        it = ui->source_1;
    else if (entry == 2)
        it = ui->source_2;
    else
        return;
    it->setCurrentText(src);
}

void FormEntry::onSourceChanged()
{
    // find sender
    QComboBox* sender_ = qobject_cast<QComboBox*>(sender());
    if (!sender_)
        return;
    if(sender_ == ui->source_1)
    {
        emit sourceChanged(UID, 1);
    }
    else if (sender_ == ui->source_2) {
        emit sourceChanged(UID, 2);
    }
    else
    {
        qDebug() << "Sender not found";
    }
}

// threshold
double FormEntry::getThreshold(int entry)
{
    if (entry == 1)
        return ui->thresholdInput_1->text().toDouble();
    else if (entry == 2)
        return ui->thresholdInput_2->text().toDouble();
    else
        return -10000;
}

void FormEntry::setThreshold(const double thre, int entry)
{
    QLineEdit* it;
    if (entry == 1)
        it = ui->thresholdInput_1;
    else if (entry == 2)
        it = ui->thresholdInput_2;
    else
        return;
    it->setText(QString::number(thre, 'f', 4));
}

void FormEntry::onThreChanged()
{
    // find sender
    QLineEdit* sender_ = qobject_cast<QLineEdit*>(sender());
    if (!sender_)
        return;
    if(sender_ == ui->thresholdInput_1)
    {
        emit thresholdCHanged(UID, 1);
    }
    else if (sender_ == ui->thresholdInput_2) {
        emit thresholdCHanged(UID, 2);
    }
    else
    {
        qDebug() << "Sender not found";
    }
}

// cali coef
double FormEntry::getCaliCoef(int entry)
{
    if (entry == 1)
        return ui->caliCoefInput_1->text().toDouble();
    else if (entry == 2)
        return ui->caliCoefInput_2->text().toDouble();
    else
        return -10000;
}

void FormEntry::setCaliCoef(const double calicoef, int entry)
{
    QLineEdit* it;
    if (entry == 1)
        it = ui->caliCoefInput_1;
    else if (entry == 2)
        it = ui->caliCoefInput_2;
    else
        return;
    it->setText(QString::number(calicoef, 'f', 4));
}

void FormEntry::onCaliCoefChanged()
{
    // find sender
    QLineEdit* sender_ = qobject_cast<QLineEdit*>(sender());
    if (!sender_)
        return;
    if(sender_ == ui->caliCoefInput_1)
    {
        emit caliCoefChanged(UID, 1);
    }
    else if (sender_ == ui->caliCoefInput_2) {
        emit caliCoefChanged(UID, 2);
    }
    else
    {
        qDebug() << "Sender not found";
    }
}
