#include "MainWindow.h"

#include <QApplication>
int main(int argc, char *argv[])
{
    qRegisterMetaTypeStreamOperators<Channel>("Channel");

    QApplication a(argc, argv);
    MainWindow w;
    w.resize(w.sizeHint());
    w.show();
    w.resize(700,700);

    return a.exec();
}
