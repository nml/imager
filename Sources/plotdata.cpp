#include "plotdata.h"
#include <fstream>
#include <iostream>
#include "omp.h"
#include "QDebug"
#include "QDateTime"

PlotData::PlotData(const Setup* config_)
      : config(config_) 
{ 
    counts = 0;
    probDist = std::vector<std::vector<double>>(config->thetaBins, std::vector<double>(config->phiBins, 0));
    pixelateSphere();

    imageData = std::vector<std::vector<double>>(config->thetaBins, std::vector<double>(config->phiBins, 0));
    
    coinSpectrum = Histogram(config->ergBins, config->ergMin, config->ergMax);

    // Pair
    for(int i=0;i<config->channelSettings.size() / 2;i++)
    {
        pairSpectra.push_back(Histogram(config->pairErgBins, config->pairErgMin, config->pairErgMax));
    }

    // LG
    for (int i=0;i<128;i++)
    {
        LGSpectra.push_back(Histogram(config->LGHGErgBins, 0, 16384));
    }

    // HG
    for (int i=0;i<128;i++)
    {
        HGSpectra.push_back(Histogram(config->LGHGErgBins, 0, 16384));
    }
}

// getters
int PlotData::getImageCounts()
{
    return counts;
}

const std::vector<std::vector<double>>& PlotData::getImageData()
{
    return imageData;
}

QVector<double> PlotData::getCoinBinCenters()
{
    return coinSpectrum.getBinCenters();
}

int PlotData::getCoinTotalCounts()
{
    return coinSpectrum.getTotalCounts();
}
QVector<double> PlotData::getCoinBinCounts()
{
    return coinSpectrum.getBinContents();
}

int PlotData::getPairTotalCounts(const int& pair)
{
    return pairSpectra[pair].getTotalCounts();
}

QVector<double> PlotData::getPairBinCenters(const int& pair)
{
    return pairSpectra[pair].getBinCenters();
}

QVector<double> PlotData::getPairBinCounts(const int& pair)
{
    return pairSpectra[pair].getBinContents();
}

int PlotData::getLGTotalCounts(const int& asic, const int& channel)
{
    int index = 32 * asic + channel;
    return LGSpectra[index].getTotalCounts();
}


QVector<double> PlotData::getLGBinCenters(const int& asic, const int& channel)
{
    int index = 32 * asic + channel;
    return LGSpectra[index].getBinCenters();
}

QVector<double> PlotData::getLGBinCounts(const int& asic, const int& channel)
{
    int index = 32 * asic + channel;
    return LGSpectra[index].getBinContents();
}

int PlotData::getHGTotalCounts(const int& asic, const int& channel)
{
    int index = 32 * asic + channel;
    return HGSpectra[index].getTotalCounts();
}

QVector<double> PlotData::getHGBinCenters(const int& asic, const int& channel)
{
    int index = 32 * asic + channel;
    return HGSpectra[index].getBinCenters();
}

QVector<double> PlotData::getHGBinCounts(const int& asic, const int& channel)
{
    int index = 32 * asic + channel;
    return HGSpectra[index].getBinContents();
}

void PlotData::rebinImage()
{
    mMutex.lock();
    counts = 0;
    probDist = std::vector<std::vector<double>>(config->thetaBins, std::vector<double>(config->phiBins, 0));
    imageData = std::vector<std::vector<double>>(config->thetaBins, std::vector<double>(config->phiBins, 0));
    pixelateSphere();
    mMutex.unlock();
}

void PlotData::rebinSpectrum()
{
    mMutex.lock();
    coinSpectrum.rebin(config->ergBins, config->ergMin, config->ergMax);
    mMutex.unlock();
//    return;
}

void PlotData::rebinPairSpectra()
{
    mMutex.lock();
    for (std::size_t i=0;i<pairSpectra.size();i++)
    {
        pairSpectra[i].rebin(config->pairErgBins, config->pairErgMin, config->pairErgMax);
    }
    mMutex.unlock();
}

void PlotData::rebinLGSpectra()
{
    mMutex.lock();
    for (std::size_t i=0;i<LGSpectra.size();i++)
    {
        LGSpectra[i].rebin(config->LGHGErgBins, 0, 16384);
    }
    mMutex.unlock();
//    return;
}

void PlotData::rebinHGSpectra()
{
    mMutex.lock();
    for (std::size_t i=0;i<HGSpectra.size();i++)
    {
        HGSpectra[i].rebin(config->LGHGErgBins, 0, 16384);
    }
    mMutex.unlock();
//    return;
}

int PlotData::pixelateSphere() {
    xbs = std::vector<std::vector<Vector3D>>(config->thetaBins, std::vector<Vector3D>(config->phiBins, Vector3D(0, 0, 0)));
    thetaBinCenters = std::vector<double>(config->thetaBins, 0);
    phiBinCenters = std::vector<double>(config->phiBins, 0);
    dphi = (config->phiMax - config->phiMin) / config->phiBins;
    dtheta = (config->thetaMax - config->thetaMin) / config->thetaBins;
    double phi = config->phiMin + dphi / 2;;
    double theta = config->thetaMin + dtheta / 2;
    for (int i = 0; i < config->thetaBins; i++)
    {
        thetaBinCenters[i] = theta;
        phi = config->phiMin + dphi / 2;
        for (int j = 0; j < config->phiBins; j++)
        {
            if (i==0)
                phiBinCenters[j] = phi;
            xbs[i][j].X = config->R * std::cos(theta) * std::cos(phi);
            xbs[i][j].Y = config->R * std::cos(theta) * std::sin(phi);
            xbs[i][j].Z = config->R * std::sin(theta);
            phi += dphi;
        }
        theta += dtheta;
    }
    return 0;
}

void PlotData::clearPairSpectra()
{
    mMutex.lock();
    for (std::size_t i=0;i<pairSpectra.size();i++)
    {
        pairSpectra[i].clear();
    }
    mMutex.unlock();
}

bool PlotData::savePairSpectrum2txt(const std::string&fileName, const int& pair)
{
    if (pair < 0 || pair >= pairSpectra.size())
        return false;
    mMutex.lock();
    bool saved=spectrum2txt(pairSpectra[pair], fileName);
    mMutex.unlock();
    return saved;
}

void PlotData::updatePairSpectra(const std::vector<MyPulse>& pulses)
{
    mMutex.lock();
    for (const MyPulse& p : pulses)
    {
        pairSpectra[p.cellNo].fill(p.height);
    }
    mMutex.unlock();
}

void PlotData::saveOnExit()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    QString currentDateTime = dateTime.toString("yyyy-MM-dd-HH-mm-ss");
    mMutex.lock();
    if (config->saveImage)
    {
        if (config->saveImageFormat == "TXT"){
            QString filename = config->outputDir.absolutePath() + QString("/Image-%1.txt").arg(currentDateTime);
            saveImage2txt(filename.toStdString());
        }
    }
    if (config->saveSpectrum)
    {
        if (config->saveImageFormat == "TXT"){
            QString filename = config->outputDir.absolutePath() + QString("/Spectrum-%1.txt").arg(currentDateTime);
            spectrum2txt(coinSpectrum, filename.toStdString());
        }
    }
    mMutex.unlock();
}

void PlotData::clearAll()
{
    mMutex.lock();
    counts=0;
    for (auto& sub : imageData) {
        std::fill(sub.begin(), sub.end(), 0);
    }
    coinSpectrum.clear();
    for (std::size_t i=0;i<pairSpectra.size();i++)
        pairSpectra[i].clear();
    for (std::size_t i=0;i<LGSpectra.size();i++)
        LGSpectra[i].clear();
    for (std::size_t i=0;i<HGSpectra.size();i++)
        HGSpectra[i].clear();
    mMutex.unlock();
}

void PlotData::clearImage()
{
    mMutex.lock();
    counts=0;
    for (auto& sub : imageData) {
        std::fill(sub.begin(), sub.end(), 0);
    }
    mMutex.unlock();
}

void PlotData::clearSpectrum()
{
    mMutex.lock();
    coinSpectrum.clear();
    mMutex.unlock();
}

void PlotData::clearLGSpectrum(const int& asic, const int& channel)
{
    int id = asic * 32 + channel;
    if(id >= LGSpectra.size())
        return;
    mMutex.lock();
    LGSpectra[id].clear();
    mMutex.unlock();
}

void PlotData::clearHGSpectrum(const int& asic, const int& channel)
{
    int id = asic * 32 + channel;
    if (id >= HGSpectra.size())
        return;
    mMutex.lock();
    HGSpectra[id].clear();
    mMutex.unlock();
}

void PlotData::clearLGHG()
{
    mMutex.lock();
    for (int i=0;i<LGSpectra.size();i++)
    {
        LGSpectra[i].clear();
    }

    for (int i=0;i<HGSpectra.size();i++)
    {
        HGSpectra[i].clear();
    }
    mMutex.unlock();
}

bool PlotData::saveImage2txt(const std::string& fileName)
{
//        qDebug() << "Save as text file";
    std::ofstream outf(fileName.c_str(), std::ios::out);
    if (!outf.good())
    {
        return false;
    }

    outf << "     Phi   Theta      Content\n";
    const double conv = 180 / M_PI;
    mMutex.lock();
    for (int i = 0; i < config->thetaBins; i++)
    {
        for (int j = 0; j < config->phiBins; j++)
        {
            outf << std::fixed    << std::setprecision(2)
                 << std::setw(8)  << thetaBinCenters[i] * conv
                    << std::setw(8)  << phiBinCenters[j] * conv
                       << std::fixed    << std::setprecision(8)
                       << std::setw(13) << imageData[i][j] << '\n';
        }
    }
    mMutex.unlock();
    outf.close();
    //    qDebug() << "Text file saved.";
    return true;
}

bool PlotData::saveLGSpectrum2txt(const std::string&fileName, const int& asic, const int& channel)
{
    int id = asic * 32 + channel;
    if(id >= LGSpectra.size())
        return false;
    mMutex.lock();
    bool saved=spectrum2txt(LGSpectra[id], fileName);
    mMutex.unlock();
    return saved;
}
bool PlotData::saveHGSpectrum2txt(const std::string&fileName, const int& asic, const int& channel)
{
    int id = asic * 32 + channel;
    if(id >= HGSpectra.size())
        return false;
    mMutex.lock();
    bool saved=spectrum2txt(HGSpectra[id], fileName);
    mMutex.unlock();
    return saved;
}

bool PlotData::saveSpectrum2txt(const std::string& fileName)
{
//        qDebug() << "Save as text file";
    mMutex.lock();
    bool saved=spectrum2txt(coinSpectrum, fileName);
    mMutex.unlock();
    return saved;
}

bool PlotData::spectrum2txt(const Histogram& spectrum, const std::string& fileName)
{
    std::ofstream outf(fileName.c_str(), std::ios::out);
    if (!outf.good())
    {
        return false;
    }

    outf << "     BinCenter      Counts\n";
    for (int i = 0; i < spectrum.getNBins(); i++)
    {
        outf << std::fixed    << std::setprecision(2)
             << std::setw(16)  << spectrum.getBinCenter(i)
             << std::setw(16)  << std::round(spectrum.getBinContent(i))<< '\n';
    }
    outf.close();
//    qDebug() << "Text file saved.";
    return true;
}

void PlotData::updateLGSpectra(const int asic, const ushort (&chargeLG)[32])
{
    mMutex.lock();
    int id = asic * 32;
    for(int i=0;i<32;i++)
    {
        if(chargeLG[i] > 0)
            LGSpectra[id+i].fill(chargeLG[i]);
    }
    mMutex.unlock();
}

void PlotData::updateHGSpectra(const int asic, const ushort (&chargeHG)[32])
{
    mMutex.lock();
    int id = asic * 32;
    for(int i=0;i<32;i++)
    {
        if(chargeHG[i] > 0)
            HGSpectra[id+i].fill(chargeHG[i]);
    }
    mMutex.unlock();
}

void PlotData::updateImage(std::vector<Cone>::const_iterator first,
                                 std::vector<Cone>::const_iterator last,
                                 const bool normalized)
{
    if (first==last)
        return;
    else if (normalized) {
        mMutex.lock();
        addConesNormalized(first, last);
        mMutex.unlock();
    }
    else {
        mMutex.lock();
        addCones(first, last);
        mMutex.unlock();
    }
}

int PlotData::addCones(std::vector<Cone>::const_iterator first,
             std::vector<Cone>::const_iterator last)
{
    // project cones onto the spherical surface
    double alpha(0);
    double beta(0);
    double E2(0);
    double sgma2(0);
    double sgmb2(0);
    Vector3D ray;
    for (auto k = first; k < last; k++)
    {
        alpha = k->cosHalfAngle;
        coinSpectrum.fill(k->E0);
        // ideal case: inital energy is known
        // sgma2 = std::pow(0.511*k->E0/std::pow((k->E0-k->Edpst),2)*config->sgmE, 2);
        // realistic case: initial energy = E_1 + E_2
        E2 = k->E0 - k->Edpst;
        sgma2 = std::pow(k->Edpst/std::pow(k->E0, 2) , 2) + std::pow(1/E2 - E2/std::pow(k->E0, 2) , 2);
        sgma2 *= std::pow(config->sgmE * 511, 2);
        #pragma omp parallel for private(ray, beta, sgmb2) collapse(2)
        for (int i = 0; i < config->thetaBins; i++)
        {
            for (int j = 0; j < config->phiBins; j++)
            {
                ray = k->apex - xbs[i][j];
                beta = getCosAngle(ray, k->axis);
                sgmb2 = std::pow((ray/(ray*ray) + k->axis/(k->axis*k->axis)-
                                (ray+k->axis)/(ray*k->axis))*config->sgmpos * beta, 2);
                sgmb2+= std::pow((ray/(ray*k->axis)-k->axis/(k->axis*k->axis))*config->sgmpos * beta, 2);
                imageData[i][j] += std::exp(-std::pow((std::pow(beta, config->order)-std::pow(alpha, config->order)), 2)/
                                            (2*std::pow(config->order, 2)*
                                             (std::pow(alpha,2*config->order-2)*sgma2 +std::pow(beta, 2*config->order-2)*sgmb2)));
            }
        }
    }
    counts += (last - first);
    return 0;
}

//int PlotData::addConesNormalized(std::vector<Cone>::const_iterator first,
//             std::vector<Cone>::const_iterator last)
//{
//    // project cones onto the spherical surface
//    double alpha(0);
//    double beta(0);
//    double E2(0);
//    double sgma2(0);
//    double sgmb2(0);
//    double summ(0);
//    Vector3D ray;
//    for (auto k = first; k < last; k++)
//    {
//        counts++;
//        if (counts == 3507)
//            qDebug() << "here";
//        alpha = k->cosHalfAngle;
//        spectrum->Fill(k->E0);
//        // ideal case: inital energy is known
//        // sgma2 = std::pow(0.511*k->E0/std::pow((k->E0-k->Edpst),2)*config->sgmE, 2);
//        // realistic case: initial energy = E_1 + E_2
//        E2 = k->E0 - k->Edpst;
//        sgma2 = std::pow(k->Edpst/std::pow(k->E0, 2) , 2) + std::pow(1/E2 - E2/std::pow(k->E0, 2) , 2);
//        sgma2 *= std::pow(config->sgmE * 0.511, 2);
//        summ = 0;
//        #pragma omp parallel for private(ray, beta, sgmb2) shared(probDist) reduction(+:summ)
//        for (int i = 0; i < config->thetaBins; i++)
//        {
//            for (int j = 0; j < config->phiBins; j++)
//            {
//                ray = k->apex - xbs[i][j];
//                beta = getCosAngle(ray, k->axis);
//                sgmb2 = std::pow((ray/(ray*ray) + k->axis/(k->axis*k->axis)-
//                                (ray+k->axis)/(ray*k->axis))*config->sgmpos * beta, 2);
//                sgmb2+= std::pow((ray/(ray*k->axis)-k->axis/(k->axis*k->axis))*config->sgmpos * beta, 2);
//                probDist[i][j] = std::exp(-std::pow((std::pow(beta, config->order)-std::pow(alpha, config->order)), 2)/
//                                (2*std::pow(config->order, 2)*
//                                (std::pow(alpha,2*config->order-2)*sgma2 +std::pow(beta, 2*config->order-2)*sgmb2)));
//                // use proability density
//                summ += probDist[i][j] * dtheta * dphi * std::cos(thetaBinCenters[i]);
//                // uncomment to use integral in the bin
//                // probDist[i][j] = probDist[i][j] * config->dtheta * config->dphi * std::cos(config->thetaBinCenters[i]);
//                // summ += probDist[i][j];
//            }
//        }
//        // #pragma omp parallel for
//        for (int i = 0; i < config->thetaBins; i++)
//        {
//            for (int j = 0; j < config->phiBins; j++)
//            {
//                hist->SetBinContent(j+1, i+1, (hist->GetBinContent(j+1, i+1)*(counts - 1) + probDist[i][j] / summ) / counts);
//                if(std::isnan(hist->GetBinContent(j+1, i+1)) || std::isinf(hist->GetBinContent(j+1,i+1)))
//                    qDebug() << i << j << counts;
//            }
//        }
//    }
//    return 0;
//}

int PlotData::addConesNormalized(std::vector<Cone>::const_iterator first,
             std::vector<Cone>::const_iterator last)
{
    // project cones onto the spherical surface
    double alpha(0);
    double beta(0);
    double E2(0);
    double sgma2(0);
//    double sgmb2(0);
//    double summ(0);
    Vector3D ray;
    double A(0);
    for (auto k = first; k < last; k++)
    {
        counts++;
        alpha = k->cosHalfAngle;
        coinSpectrum.fill(k->E0);
        // ideal case: inital energy is known
        // sgma2 = std::pow(0.511*k->E0/std::pow((k->E0-k->Edpst),2)*config->sgmE, 2);
        // realistic case: initial energy = E_1 + E_2
        E2 = k->E0 - k->Edpst;
        sgma2 = std::pow(k->Edpst/std::pow(k->E0, 2) , 2) + std::pow(1/E2 - E2/std::pow(k->E0, 2) , 2);
        sgma2 *= std::pow(config->sgmE * 511, 2);
        A = 0.5 * M_1_PI * M_2_SQRTPI * M_SQRT1_2 / std::sqrt(sgma2) / (std::erf((1-alpha)/ (M_SQRT2 * std::sqrt(sgma2))) - std::erf((-1-alpha)/ (M_SQRT2 * std::sqrt(sgma2))));
//        #pragma omp parallel for private(ray, beta) shared(probDist)
        for (int i = 0; i < config->thetaBins; i++)
        {
            for (int j = 0; j < config->phiBins; j++)
            {
                ray = k->apex - xbs[i][j];
                beta = getCosAngle(ray, k->axis);

                probDist[i][j] = A * std::exp(-std::pow(beta-alpha, 2)/
                                (2*sgma2));
            }
        }
        // #pragma omp parallel for
        for (int i = 0; i < config->thetaBins; i++)
        {
            for (int j = 0; j < config->phiBins; j++)
            {
                imageData[i][j] = (imageData[i][j] *(counts-1) + probDist[i][j]) / counts;
//                if(std::isnan(hist->GetBinContent(j+1, i+1)) || std::isinf(hist->GetBinContent(j+1,i+1)))
//                    qDebug() << i << j << counts;
            }
        }
    }
    return 0;
}

